﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LauncherUpdater
{

    public partial class Main : Form
    {
#if GAME_IS_GREENHELL
        static string DISCORD_INVITEURL = "discord.gg/Qgdqmcy";
        static string MODLOADER_NAME_SHORT = "GHML";
        static string MODLOADER_NAME_NOSPACES = "GreenHellModLoader";
        static string MODLOADER_FASTDL_URL = "https://fastdl.greenhellmodding.com";
#elif GAME_IS_RAFT
        static string DISCORD_INVITEURL = "discord.gg/Dwz7GSA";
        static string MODLOADER_NAME_SHORT = "RML";
        static string MODLOADER_NAME_NOSPACES = "RaftModLoader";
        static string MODLOADER_FASTDL_URL = "https://fastdl.raftmodding.com";
#endif

        string currentExeHash;
        string dataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES);
        string launcherExecutable = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES + "/HMLCore.exe");
        string arguments;

        ModLoaderVersion latestUpdateInfo;

        public Main()
        {
            string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            if (path.ToUpperInvariant() == dataFolder.ToUpperInvariant())
            {
                MessageBox.Show("An error occured while starting the launcher!\n\nIt seems like the launcher autoupdater executable is located in the " + MODLOADER_NAME_NOSPACES + " folder.\n\nPlease try to place it somewhere else and if the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                Application.Exit();
            }

            arguments = string.Join(" ", Environment.GetCommandLineArgs().Skip(1));
            InitializeComponent();
            InitializeTheme();
            statusLabel.Size = new Size(400, 80);
            statusLabel.Location = new Point(0, 0);
            statusLabel.TextAlign = ContentAlignment.MiddleCenter;
            backgroundBar.Hide();
            progressLabel.Hide();
            statusLabel.Text = "Checking for launcher updates...";
            Directory.CreateDirectory(dataFolder);
            if (File.Exists(launcherExecutable))
            {
                currentExeHash = CalculateSHA512(launcherExecutable);
            }

            if (arguments.ToUpperInvariant().Contains("-DEBUG"))
            {
                if (File.Exists(launcherExecutable))
                {
                    StartLauncher();
                }
            }
            else
            {
                CheckForUpdates();
            }
        }

        void InitializeTheme()
        {
#if GAME_IS_GREENHELL
            BackColor = System.Drawing.Color.FromArgb(37, 37, 37);
            progressBar.BackColor = System.Drawing.Color.FromArgb(185, 29, 35);
            backgroundBar.BackColor = System.Drawing.Color.FromArgb(65, 65, 65);
#elif GAME_IS_RAFT
            Icon = LauncherUpdater.Properties.Resources.rml_logo;
            BackColor = System.Drawing.Color.FromArgb(69, 90, 100);
            progressBar.BackColor = System.Drawing.Color.FromArgb(129, 199, 132);
            backgroundBar.BackColor = System.Drawing.Color.FromArgb(55, 71, 79);
#endif
        }

        async void StartLauncher()
        {
            if (File.Exists(launcherExecutable))
            {
                Process.Start(launcherExecutable);
            }
            else
            {
                MessageBox.Show("An error occured while starting the launcher!\nThe launcher executable doesn't exist!\nPlease try to restart the launcher and if the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
            Process.GetCurrentProcess().Kill();
        }

        async void CheckForUpdates()
        {
            try
            {
                string ConfigFile = Path.Combine(dataFolder, "config.ini");
                string currentBranch = "public";
                try
                {
                    if (File.Exists(ConfigFile))
                    {
                        var parser = new FileIniDataParser();
                        IniData data = parser.ReadFile(ConfigFile);
                        currentBranch = data["launcher"]["branch"];
                    }
                }
                catch { currentBranch = "public"; }

                Task<string> task = RequestData(new Uri(MODLOADER_FASTDL_URL + "/data/launcher.php?branch=" + currentBranch));
                var waitTask = Task.Delay(15000);
                Exception error = new Exception(MODLOADER_FASTDL_URL + "/data/launcher.php?branch=" + currentBranch + " Timed Out!\n\nPLEASE NOTE, WE CAN NOT HELP YOU WITH THIS PROBLEM, It is probably related to your antivirus or windows defender, your firewall or your ISP (internet provider) in some extremely rare cases.");
                if (await Task.WhenAny(task, waitTask).ConfigureAwait(true) == task)
                {

                    if (task.Result.Length > 10 && !task.IsFaulted && task.Exception == null && !task.IsCanceled && task.IsCompleted)
                    {
                        latestUpdateInfo = ReadModLoaderVersion(task.Result);
                        if (currentExeHash != latestUpdateInfo.hash)
                        {
                            Hide();
                            UpdateAvailable form = new UpdateAvailable(latestUpdateInfo);
                            DialogResult result = form.ShowDialog();
                            if (result == DialogResult.Yes)
                            {
                                Show();
                                statusLabel.Text = "Downloading " + MODLOADER_NAME_SHORT + "Launcher " + latestUpdateInfo.version + "...";
                                UpdateLauncher();
                            }
                            else
                            {
                                Application.Exit();
                            }
                        }
                        else
                        {
                            StartLauncher();
                        }
                    }
                    else
                    {
                        if (task.Exception != null)
                        {
                            MessageBox.Show("An error occured while fetching for updates\n" + FlattenException(task.Exception) + "\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show("An error occured while fetching for updates\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                        }
                        Application.Exit();
                    }
                }
                else
                {
                    MessageBox.Show("An error occured while fetching for updates\n" + FlattenException(error) + "\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("An error occured while fetching for updates\n" + FlattenException(e) + "\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                if (!File.Exists(launcherExecutable))
                {
                    Application.Exit();
                }
                else
                {
                    StartLauncher();
                }
            }
        }

        public static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }
            return stringBuilder.ToString();
        }

        void UpdateLauncher()
        {
            if (latestUpdateInfo.url.EndsWith(".exe", StringComparison.InvariantCulture))
            {
                statusLabel.Text = "Downloading Launcher " + latestUpdateInfo.version + "...";
                statusLabel.Size = new Size(301, 30);
                statusLabel.Location = new Point(10, 9);
                statusLabel.TextAlign = ContentAlignment.MiddleLeft;
                progressBar.Size = new Size(0, 30);
                backgroundBar.Show();
                progressLabel.Show();
                progressLabel.Text = "0%";
                WebClient launcherExeClient = new WebClient();
                launcherExeClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(LauncherExeProgressChanged);
                launcherExeClient.DownloadFileCompleted += new AsyncCompletedEventHandler(LauncherExeDownloadCompleted);
                launcherExeClient.DownloadFileAsync(new Uri(latestUpdateInfo.url), launcherExecutable);
            }
        }

        private void LauncherExeProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double bytesIn = e.BytesReceived;
            double totalBytes = e.TotalBytesToReceive;
            double percentage = bytesIn / totalBytes * 100;
            progressLabel.Text = Math.Round(percentage) + "%";
            float progress = (376f / 100f) * (float)Math.Round(percentage);
            progressBar.Size = new Size((int)Math.Floor(progress), 30);
        }

        private async void LauncherExeDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            statusLabel.Text = "Unblocking the downloaded files...";

            await UnblockModLoaderDirectoryFiles();

            statusLabel.Text = "Installing the latest launcher update...";
            try
            {
                backgroundBar.Hide();
                progressBar.Hide();
                progressLabel.Text = "";
                statusLabel.TextAlign = ContentAlignment.MiddleCenter;
                statusLabel.Size = new Size(376, 65);

                if (File.Exists(launcherExecutable))
                {
                    if (CalculateSHA512(launcherExecutable).Equals(latestUpdateInfo.hash, StringComparison.InvariantCulture))
                    {
                        StartLauncher();
                    }
                    else
                    {
                        string currentHash = CalculateSHA512(launcherExecutable);
                        File.Delete(launcherExecutable);
                        MessageBox.Show("An error occured while downloading the latest launcher! The downloaded file has a different hash than the requested file!\n\nLocal HMLCore.exe Hash = " + currentHash + "\nRequested HMLCore.exe Hash = " + latestUpdateInfo.hash + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Application.Exit();
                    }
                }
                else
                {
                    MessageBox.Show("An error occured while downloading the latest launcher! The downloaded file doesn't exists! It might have been removed by an antivirus software or something else.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while installing the latest launcher update!\n" + FlattenException(ex) + "\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
            }
            await Task.Delay(1000);
            MessageBox.Show("An error occured while installing the latest launcher update!\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            Process.GetCurrentProcess().Kill();
        }

        static string CalculateSHA512(string filename)
        {
            if (!File.Exists(filename))
            {
                return "FILE DOESN'T EXIST!";
            }
            using (var sha512 = SHA512.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = sha512.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToUpperInvariant();
                }
            }
        }

        public static async Task<string> RequestData(Uri uri)
        {
            WebDownload wd = new WebDownload();
            wd.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            string result = await wd.DownloadStringTaskAsync(uri);
            wd.Dispose();
            return result;
        }

        public static ModLoaderVersion ReadModLoaderVersion(string json)
        {
            ModLoaderVersion deserializedModLoaderVersion = new ModLoaderVersion();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedModLoaderVersion.GetType());
            deserializedModLoaderVersion = ser.ReadObject(ms) as ModLoaderVersion;
            ms.Close();
            return deserializedModLoaderVersion;
        }

        bool mouseDown;
        int mouseX = 0, mouseY = 0;
        int mouseinX = 0, mouseinY = 0;

        private void DragMouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseinX = MousePosition.X - Bounds.X;
            mouseinY = MousePosition.Y - Bounds.Y;
        }

        private void DragMouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - mouseinX;
                mouseY = MousePosition.Y - mouseinY;

                SetDesktopLocation(mouseX, mouseY);
            }
        }

        public async Task UnblockModLoaderDirectoryFiles()
        {
            try
            {
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES + "/*");

                ProcessStartInfo startInfo = new ProcessStartInfo("Powershell.exe");
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = "dir -Recurse \"" + path + "\" | Unblock-File";

                Process.Start(startInfo);
                await Task.Delay(200);
            }
            catch { }
        }
    }

    public class WebDownload : WebClient
    {
        public int Timeout { get; set; }

        public WebDownload() : this(1) { }

        public WebDownload(int timeout)
        {
            this.Timeout = timeout;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);
            if (request != null)
            {
                request.Timeout = this.Timeout;
            }
            return request;
        }
    }

    public class ModLoaderVersion
    {
        public string version = "";
        public string url = "";
        public string hash = "";
        public string rawChangelog = "";
        public string fullChangelogUrl = "";
    }
}