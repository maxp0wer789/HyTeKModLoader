﻿namespace LauncherUpdater
{
    partial class Main
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.statusLabel = new System.Windows.Forms.Label();
            this.backgroundBar = new System.Windows.Forms.Panel();
            this.progressBar = new System.Windows.Forms.Panel();
            this.progressLabel = new System.Windows.Forms.Label();
            this.backgroundBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusLabel
            // 
            this.statusLabel.BackColor = System.Drawing.Color.Transparent;
            this.statusLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.statusLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusLabel.ForeColor = System.Drawing.Color.White;
            this.statusLabel.Location = new System.Drawing.Point(12, 9);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(301, 30);
            this.statusLabel.TabIndex = 0;
            this.statusLabel.Text = "Updating Launcher...";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.statusLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.statusLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.statusLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // backgroundBar
            // 
            this.backgroundBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.backgroundBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.backgroundBar.Controls.Add(this.progressBar);
            this.backgroundBar.Location = new System.Drawing.Point(12, 42);
            this.backgroundBar.Name = "backgroundBar";
            this.backgroundBar.Size = new System.Drawing.Size(376, 30);
            this.backgroundBar.TabIndex = 1;
            this.backgroundBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.backgroundBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.backgroundBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(29)))), ((int)(((byte)(35)))));
            this.progressBar.Location = new System.Drawing.Point(0, 0);
            this.progressBar.Margin = new System.Windows.Forms.Padding(0);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(376, 30);
            this.progressBar.TabIndex = 2;
            this.progressBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.progressBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.progressBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // progressLabel
            // 
            this.progressLabel.BackColor = System.Drawing.Color.Transparent;
            this.progressLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.progressLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressLabel.ForeColor = System.Drawing.Color.White;
            this.progressLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.progressLabel.Location = new System.Drawing.Point(329, 9);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(59, 30);
            this.progressLabel.TabIndex = 3;
            this.progressLabel.Text = "100%";
            this.progressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.progressLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.progressLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.progressLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(400, 80);
            this.Controls.Add(this.progressLabel);
            this.Controls.Add(this.backgroundBar);
            this.Controls.Add(this.statusLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TopMost = true;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            this.backgroundBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Panel backgroundBar;
        private System.Windows.Forms.Panel progressBar;
        private System.Windows.Forms.Label progressLabel;
    }
}

