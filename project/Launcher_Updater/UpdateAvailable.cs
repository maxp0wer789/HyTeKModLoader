﻿using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace LauncherUpdater
{
    public partial class UpdateAvailable : Form
    {
        ModLoaderVersion latestUpdateInfo;

        public UpdateAvailable(ModLoaderVersion version)
        {
            InitializeComponent();
            latestUpdateInfo = version;
            versionLabel.Text = "Launcher " + version.version;
            changelog.Text = version.rawChangelog;
            InitializeTheme();
        }

        void InitializeTheme()
        {
#if GAME_IS_RAFT
            logo.BackgroundImage = Properties.Resources.raft_rml_logo;
            BackColor = Color.FromArgb(55, 71, 79);
            changelog.BackColor = Color.FromArgb(84, 110, 122);
#endif
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(latestUpdateInfo.fullChangelogUrl);
        }
    }
}
