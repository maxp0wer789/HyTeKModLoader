﻿using HMLLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GHML
{
    public class DefaultConsoleCommands
    {
        public static Dictionary<KeyCode, string> boundCommands = new Dictionary<KeyCode, string>();

        public static void UpdateBoundCommandsFile()
        {
            JSONObject j = new JSONObject(JSONObject.Type.ARRAY);
            foreach (KeyValuePair<KeyCode, string> v in boundCommands)
            {
                j.Add(v.Key.ToString() + "•◆•◆•" + v.Value);
            }
            PlayerPrefs.SetString("ghml.boundcommands", j.Print());
        }

        public static void LoadBoundCommands()
        {
            if (PlayerPrefs.HasKey("ghml.boundcommands"))
            {
                JSONObject j = new JSONObject(PlayerPrefs.GetString("ghml.boundcommands"));
                if (j.IsArray && j.list.Count >= 1)
                {
                    foreach (JSONObject obj in j.list)
                    {
                        try
                        {
                            string[] args = obj.str.Split(new[] { "•◆•◆•" }, StringSplitOptions.None);
                            if (args.Length < 2)
                            {
                                Debug.LogWarning("You must specify a key and a command as arguments to 'bind'.");
                                return;
                            }

                            KeyCode key = HUtils.KeyCodeFromString(args[0]);
                            if (key == KeyCode.None) { return; }
                            string command = string.Join(" ", args.Skip(1).ToArray());
                            boundCommands[key] = command;
                            UpdateBoundCommandsFile();
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                    }
                    Debug.Log("Successfully loaded bound commands.");
                }
            }
        }

        [ConsoleCommand("bind", "Syntax: 'bind <key> <command...>' Bind a console command to a key.")]
        public static void BindCommand(string[] args)
        {
            if (args.Length < 2)
            {
                Debug.LogWarning("You must specify a key and a command as arguments to 'bind'.");
                return;
            }

            KeyCode key = HUtils.KeyCodeFromString(args[0]);
            if (key == KeyCode.None) { return; }
            string command = string.Join(" ", args.Skip(1).ToArray());
            boundCommands[key] = command;
            UpdateBoundCommandsFile();
        }

        [ConsoleCommand("unbind", "Syntax: 'unbind <key>' Unbind a console command from a key.")]
        public static void UnbindCommand(string[] args)
        {
            if (args.Length < 1)
            {
                Debug.LogWarning("Command 'unbind' only takes 1 argument.");
                return;
            }

            KeyCode key = HUtils.KeyCodeFromString(args[0]);
            if (key == KeyCode.None) { return; }
            boundCommands.Remove(key);
            UpdateBoundCommandsFile();
        }

        [ConsoleCommand("unbindall", "Unbind all keys.")]
        public static void Unbindall()
        {
            boundCommands.Clear();
            UpdateBoundCommandsFile();
        }

        [ConsoleCommand("unityVersion", "Shows you the unity version the game use.")]
        public static string UnityVersion()
        {
            return "Current Unity Version: " + Application.unityVersion;
        }

        [ConsoleCommand("timeset", "Syntax: 'timeset <hour>' Change the game time.")]
        public static void Timeset(string[] args)
        {
            if (SceneManager.GetActiveScene().name == "MainMenu")
            {
                Debug.LogWarning("You can't set the time in the main menu.");
                return;
            }
            if (args.Length != 1)
            {
                Debug.LogWarning("You must specify a value to change the game time. Syntax: 'timeset <hour>'");
            }
            else
            {
                int newValue = -1;
                int.TryParse(args[0], out newValue);

                if (newValue < 0 || newValue > 24)
                {
                    Debug.LogWarning("The value that you specified is invalid, It should be between 0 and 24!");
                    return;
                }
                else
                {
                    MainLevel.Instance.m_TODSky.Cycle.Hour = newValue;
                    Debug.Log("The game time has been changed to " + newValue);
                }

            }
        }

        [ConsoleCommand("ghVersion", "Shows you the current Green Hell Version.")]
        public static string GreenhellVersion()
        {
            return "Current Green Hell Version: " + GreenHellGame.s_GameVersion.WithBuildVersionToString();
        }

        [ConsoleCommand("dotnetVersion", "Shows you the .NET version the game use.")]
        public static string dotnetVersion()
        {
            return "Current .NET Version: " + Environment.Version.ToString();
        }

        [ConsoleCommand("clear", "Clear the current console output.")]
        public static void clear()
        {
            GHConsole.ClearConsole();
        }

        [ConsoleCommand("kill", "Kill yourself.")]
        public static void kill()
        {
            if (Player.Get() != null)
            {
                Player.Get().Kill();
                Debug.Log("The suicide note was written in blue ink.");
                return;
            }
            Debug.LogWarning("You can't suicide right now.");
        }

        [ConsoleCommand("heal", "Heal yourself.")]
        public static void heal()
        {
            if (Player.Get() != null)
            {
                Player.Get().ScenarioHeal();
                Player.Get().ScenarioHeal();
                Debug.Log("You healed yourself.");
                P2PSession.Instance.SendTextChatMessage("I healed myself using cheats.");
                return;
            }
            Debug.LogWarning("You can't heal right now.");
        }

        [ConsoleCommand("fullscreenMode", "Syntax : 'fullscreenMode windowed/fullscreen/borderless' Set the current fullscreen mode.")]
        public static void fullscreen(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Syntax error! Usage: <u><i>fullscreenMode windowed/fullscreen/borderless</i></u>");
                return;
            }
            string mode = args[0].ToLower();
            switch (mode)
            {
                case "windowed":
                    Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.Windowed);
                    break;
                case "fullscreen":
                    Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.ExclusiveFullScreen);
                    break;
                case "borderless":
                    Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.FullScreenWindow);
                    break;
                default:
                    Debug.LogWarning("Invalid fullscreen mode, Valid modes are <i>Windowed</i>, <i>Fullscreen</i> or <i>Borderless</i>");
                    break;
            }
        }

        [ConsoleCommand("settimescale", "Syntax: 'settimescale <speed>' Change the scale at which time passes.")]
        public static void Settimescale(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("You must specify a value to change the game speed.");
                return;
            }

            int newValue = 1;
            int.TryParse(args[0], out newValue);

            if (newValue < 0 || newValue > 100)
            {
                Debug.LogWarning("The value that you specified is invalid, It should be between 0 and 100!");
                return;
            }
            else
            {
                Time.timeScale = newValue;
                Debug.Log("The game timescale has been changed to " + newValue);
            }
        }

        [ConsoleCommand("exit", "Exit the game.")]
        public static void exit()
        {
            Application.Quit();
        }

        [ConsoleCommand("getcurrentlevel", "Tells you the current game level.")]
        public static string getcurrentlevel()
        {
            return "Current Level : " + SceneManager.GetActiveScene().name;
        }

        [ConsoleCommand("loadlevel", "Syntax: 'loadlevel <value>' Load the specified level.")]
        public static void loadlevel(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Invalid Arguments! Syntax: 'loadlevel <value>'");
                return;
            }
            else
            {
                SceneManager.LoadScene(args[0]);
            }
        }

        [ConsoleCommand("poopmode", "Makes the game looks like minecraft but it helps to play the game on low-end devices such as old laptops.")]
        public static void PoopMode()
        {
            QualitySettings.pixelLightCount = 0;
            QualitySettings.masterTextureLimit = 2;
            QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
            QualitySettings.antiAliasing = 0;
            QualitySettings.softParticles = false;
            QualitySettings.realtimeReflectionProbes = false;
            QualitySettings.billboardsFaceCameraPosition = false;
            QualitySettings.shadowCascades = 0;
            QualitySettings.shadows = ShadowQuality.Disable;
            QualitySettings.softVegetation = false;
            QualitySettings.vSyncCount = 0;
            //QualitySettings.lodBias = 0;
            QualitySettings.maximumLODLevel = 1;
            QualitySettings.shadowDistance = 0;
            QualitySettings.maxQueuedFrames = 1000;

            Application.backgroundLoadingPriority = ThreadPriority.Low;
            Application.runInBackground = true;
        }
    }
}