﻿using HMLLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace GHML
{
    public class HomePage : MenuPage
    {
        public static HomePage instance;
        public static Text RandomText;
        public static Transform featuredMods;

        public static string[] randomTexts = {
        "This text isn't to fill blank space.",
        "Got any mini shields?",
        "This text doesn't exist!",
        "I am out of Ideas.",
        "Wait are you actually reading this?!",
        "Out of messages ideas.",
        "Is this the Krusty Krab??",
        "Welcome to meme land!",
        "Is your mom a pickaxe?",
        "So sad! Alexa, launch green hell.",
        "Publishing Half-Life 3... Well we have HL:Alyx..",
        "Do not turn the Power off or remove the Memory Card! ",
        "Leeroooooooooy Jenkins !",
        "Approaching the wild Wumpus.",
        "You do not currently own this DLC",
        "Eating cereal with a fork.",
        "Are You Ready Kids?",
        "Joining the battle.",
        "Shopping for more features.",
        "The cake is a lie.",
        "Deja vu, I’ve just been in this place before!",
        "Setting fire to the rain...",
        "Do you even read these?",
        "We need more power!",
        "Green Hell: Battle Royale is coming soon!",
        "Are you feeling it now?",
        "Feeding the Wumpus...",
        "Do you know da wae?",
        "Fighting the Fake News -Donald J. Trump",
        "It's prime time b*tch!",
        "Mining those diamonds!",
        "Hold my beer.",
        "Believing in the You that believes in me.",
        "Telling Ghost Pepper to Scream Passion!",
        "Entering The Oasis...",
        "Scoopty-Whooping...",
        "Calibrating the France!",
        "My name is your mom's name.",
        "Team Rocket is blasting off again!",
        "/ban @everyone",
        "Playing past bedtime.",
        "Preparing for the boss...",
        "Pressing buttons - BEEP BOOP!",
        "Eyeballing the clock!",
        "Take me homeeee...Country Roadddddd...",
        "Hmph, I'm the coolest!",
        "MarshMello is playing RocketLeague!",
        "Real People, Not Actors",
        "FAILING TO DISARM BOMB!!",
        "Finding a better Tek",
        "Did xTBolt Finish his castle",
        "Tek is a bad coder btw",
        "Deleting FalseHope",
        "Programming the flux capacitor",
        "would you like fries with that?",
        "The last time I tried this the monkey didn't survive. Let's hope it works better this time",
        "is that Minecraft?",
        "Bruce could be your daddy",
        "OOF",
        "Press Alt+F4 for a quick IQ test",
        "Loading completed. Press F13 to continue.",
        "Water detected on drive C:, please wait. Spin dry commencing.",
        "I'm quite drunk, loading might take a little more time than the usual! Please be patient!",
        "Loading the Loading message..",
        "Calling police.....",
        "Instaling windows 98",
        "Are you mad?",
        "Please, if you enjoy our mods then give us ur moms credit card",
        "Hey, i can fly!",
        "What's the matter? You got somethin' in your eye?",
        "We have to go. I'm almost happy here.",
        "Lemme tell you a story...",
        "Floor is lava!"
    };

        public override async void Initialize()
        {
            instance = this;
            RandomText = transform.Find("RandomText").Find("text").GetComponent<Text>();
            RandomText.text = randomTexts[UnityEngine.Random.Range(0, randomTexts.Length - 1)];
            featuredMods = transform.Find("FeaturedMods");
            transform.Find("Links").Find("website").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.greenhellmodding.com"));
            transform.Find("Links").Find("discord").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://discord.gg/Qgdqmcy"));
            transform.Find("Links").Find("patreon").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.patreon.com/bePatron?u=13242555"));
            transform.Find("Links").Find("documentation").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://docs.greenhellmodding.com"));
            transform.Find("Links").Find("facebook").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.facebook.com/GreenHellModding"));
        }

        public async override void OnPageOpen()
        {
            RandomText.text = randomTexts[UnityEngine.Random.Range(0, randomTexts.Length - 1)];
            try
            {
                string homepageinfo = await new WebClient().DownloadStringTaskAsync("https://fastdl.greenhellmodding.com/data/homepage.json");
                Homepageinfo t = JsonConvert.DeserializeObject<Homepageinfo>(homepageinfo);
                ProcessHomepageServerInfo(t);
            }
            catch { }
        }

        void ProcessHomepageServerInfo(Homepageinfo e)
        {
            for (int i = 1; i < 6; i++)
            {
                int id = i - 1;
                if (e.featuredMods.Count > id)
                {
                    featuredMods.Find("modlist").Find("FeaturedMod" + i).Find("Name").GetComponent<Text>().text = e.featuredMods[i - 1].title;
                    featuredMods.Find("modlist").Find("FeaturedMod" + i).Find("Description").GetComponent<Text>().text = e.featuredMods[i - 1].description;
                    featuredMods.Find("modlist").Find("FeaturedMod" + i).Find("viewmod").GetComponent<Button>().interactable = true;
                    featuredMods.Find("modlist").Find("FeaturedMod" + i).Find("viewmod").GetComponent<Button>().onClick.RemoveAllListeners();
                    string url = e.featuredMods[i - 1].id;
                    featuredMods.Find("modlist").Find("FeaturedMod" + i).Find("viewmod").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.greenhellmodding.com/mods/" + url));

                    RawImage img = featuredMods.Find("modlist").Find("FeaturedMod" + i).Find("ModIcon").Find("Icon").GetComponent<RawImage>();
                    HUtils.DownloadUncachedTexture(e.featuredMods[i - 1].iconImageUrl).ContinueWith((t) =>
                      {
                          img.texture = t.Result;
                      });
                }
                else
                {
                    featuredMods.Find("modlist").Find("FeaturedMod" + i).gameObject.SetActive(false);
                }
            }

            string readme = e.latestChangelog.readme.Replace("\\r", "\r").Replace("\\n", "\n");
            transform.Find("News").Find("Description").GetComponent<Text>().text = readme;
            transform.Find("News").Find("Name").GetComponent<Text>().text = "GreenHellModLoader " + e.latestChangelog.rmlVersion;
            transform.Find("News").Find("Time").GetComponent<Text>().text = HUtils.GetDateFormatted(e.latestChangelog.createdAt);
            transform.Find("News").Find("view").GetComponent<Button>().onClick.RemoveAllListeners();
            transform.Find("News").Find("view").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.greenhellmodding.com/loader/" + e.latestChangelog.rmlVersion));
        }
    }
}

[Serializable]
class Homepageinfo
{
    public List<FeaturedMod> featuredMods = new List<FeaturedMod>();
    public LatestChangelog latestChangelog = new LatestChangelog();
}
[Serializable]
class FeaturedMod
{
    public string id = "";
    public string title = "";
    public string description = "";
    public string iconImageUrl = "";
}
[Serializable]
class LatestChangelog
{
    public string rmlVersion = "";
    public string createdAt = "";
    public string readme = "";
}