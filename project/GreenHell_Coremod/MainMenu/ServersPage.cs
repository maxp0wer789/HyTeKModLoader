﻿using HarmonyLib;
using HMLLibrary;
using Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace GHML
{
    public class ServersPage : MenuPage
    {
        public static ServersPage instance;
        public static Button serverlistRefreshBtn;
        public static GameObject serverlistContent;
        public static GameObject ServerEntryPrefab;
        public static Text serverlistStatus;
        public static Text serversAmount;
        public static Text serversAmountTooltip;
        public static string searchServersValue = "";

        public override async void Initialize()
        {
            instance = this;
            ServerEntryPrefab = await HLib.bundle.TaskLoadAssetAsync<GameObject>("ServerEntry");
            serverlistContent = transform.Find("ServerScrollView").Find("Viewport").Find("GHMLServerListContent").gameObject;
            serverlistStatus = transform.Find("ServerScrollView").Find("Viewport").Find("StatusText").GetComponent<Text>();

            transform.Find("KnowMoreGHDS").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.greenhellmodding.com"));
            serverlistRefreshBtn = transform.Find("GHML_RefreshServerList").GetComponent<Button>();
            serverlistRefreshBtn.onClick.AddListener(RefreshServerList);
            transform.Find("SearchBar").GetComponent<InputField>().onValueChanged.AddListener((val) => OnSearchServersInputChanged(val));
            transform.Find("SearchBar").GetComponent<InputField>().onEndEdit.AddListener((val) => OnSearchServersInputChanged(val));
            serversAmount = GHMainMenu.instance.gameObject.transform.Find("BG").Find("LeftBar").Find("GHMLMainMenu_Slots").Find("GHMLMainMenuLButton_Servers").Find("Amount").Find("AmountValue").GetComponent<Text>();
            serversAmountTooltip = GHMainMenu.instance.gameObject.transform.Find("BG").Find("LeftBar").Find("GHMLMainMenu_Slots").Find("GHMLMainMenuLButton_Servers").Find("Amount").Find("Tooltip").GetComponentInChildren<Text>();
            serversAmount.gameObject.AddComponent<TooltipHandler>().tooltip = GHMainMenu.instance.gameObject.transform.Find("BG").Find("LeftBar").Find("GHMLMainMenu_Slots").Find("GHMLMainMenuLButton_Servers").Find("Amount").Find("Tooltip").gameObject;

            P2PTransportLayer.OnLobbyListAcquiredEvent += ProcessServerList;
            P2PTransportLayer.Instance.RequestLobbyList(P2PGameVisibility.Public | P2PGameVisibility.Friends | P2PGameVisibility.Private | P2PGameVisibility.Singleplayer);
        }

        public static void OnSearchServersInputChanged(string val)
        {
            searchServersValue = val;
            foreach (Transform t in serverlistContent.transform)
            {
                t.gameObject.SetActive(HUtils.StripRichText(t.Find("Name").GetComponent<Text>().text).ToLower().Contains(val.ToLower()));
            }
        }

        public async void RefreshServerList()
        {
            foreach (Transform t in serverlistContent.transform)
            {
                Destroy(t.gameObject);
            }
            serverlistStatus.text = "Retrieving servers...";
            P2PTransportLayer.Instance.RequestLobbyList(P2PGameVisibility.Public);
            serverlistRefreshBtn.interactable = false;
            await Task.Delay(2000);
            serverlistRefreshBtn.interactable = true;
        }

        public async void ProcessServerList(List<P2PLobbyInfo> lobbies)
        {
            serverlistStatus.text = "Processing servers...";
            transform.Find("SearchBar").GetComponent<InputField>().text = "";
            foreach (Transform t in serverlistContent.transform)
            {
                Destroy(t.gameObject);
            }
            serverlistStatus.text = "";
            int serversAmount = lobbies.Count;
            if (serversAmount <= 0)
            {
                serverlistStatus.text = "No servers found!";
            }
            ServersPage.serversAmount.text = serversAmount.ToString();
            if (serversAmount <= 0)
            {
                ServersPage.serversAmountTooltip.text = "No Servers";
            }
            else if (serversAmount == 1)
            {
                ServersPage.serversAmountTooltip.text = serversAmount + " Server";
            }
            else
            {
                ServersPage.serversAmountTooltip.text = serversAmount + " Servers";
            }

            lobbies.ForEach((server) =>
            {
                GameObject item = Instantiate(ServerEntryPrefab, Vector3.zero, Quaternion.identity, serverlistContent.transform);
                string serverName = server.m_Name + "'s party";
                string serverPlayers = server.m_MemberCount + "/" + server.m_SlotCount;
                item.transform.Find("Name").GetComponent<Text>().text = serverName;
                item.transform.Find("Players").GetComponent<Text>().text = serverPlayers;
                item.transform.Find("Gamemode").GetComponent<Text>().text = server.m_GameMode.ToString();
                item.transform.Find("Buttons").Find("ConnectBtn").gameObject.AddComponent<TooltipHandler>().tooltip = item.transform.Find("Buttons").Find("ConnectBtn").Find("Tooltip").gameObject;
                item.transform.Find("Buttons").Find("ConnectBtn").GetComponent<Button>().onClick.RemoveAllListeners();
                item.transform.Find("Buttons").Find("ConnectBtn").GetComponent<Button>().onClick.AddListener(() => ConnectToServer(server));
            });
        }

        public static void ConnectToServer(P2PLobbyInfo lobbyinfo)
        {
            GreenHellGame.Instance.m_SessionJoinHelper.Join(lobbyinfo.m_Address);
            if (MenuInGameManager.Get())
            {
                MenuInGameManager.Get().HideMenu();
                return;
            }
            GHMainMenu.CloseMenu();
        }
    }
}
