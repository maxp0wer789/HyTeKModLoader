﻿using HarmonyLib;
using HMLLibrary;
using RaftModLoader;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEngine;
using Debug = UnityEngine.Debug;
using System.Linq;
using Steamworks;
using UnityEngine.SceneManagement;

public class RML_Main : MonoBehaviour
{
    private bool debug = false;

    public static List<string> logs = new List<string>();

    public static KeyCode MenuKey = KeyCode.F9;
    public static KeyCode ConsoleKey = KeyCode.F10;

    [Obsolete("Please use HLib.path_modsFolder instead")]
    public static string path_modsFolder = Path.Combine(Application.dataPath, "..\\mods");

    void OnEnable()
    {
        Application.logMessageReceived += HandleUnityLog;
    }
    void HandleUnityLog(string logString, string stackTrace, LogType type)
    {
        if (debug)
        {
            logs.Add(logString + "\n" + stackTrace);
        }
    }

    void OnGUI()
    {
        if (debug)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.red;
            string text = "";
            foreach (string s in logs)
            {
                text += "\n" + s;
            }
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), text, style);
        }
    }

    private void OnSceneChanged(Scene scene1, Scene scene2)
    {
        Raft_Network.OnJoinResult += OnJoinResult;
    }

    public static async void OnJoinResult(CSteamID remoteID, InitiateResult result)
    {
        ServersPage.OnJoinResult(remoteID, result);
        NetworkModManager.OnJoinResult(remoteID, result);
    }

    private void Awake()
    {
        SceneManager.activeSceneChanged += OnSceneChanged;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        Directory.CreateDirectory(HLib.path_dataFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder_mods);
        Directory.CreateDirectory(HLib.path_cacheFolder_textures);
        Directory.CreateDirectory(HLib.path_cacheFolder_temp);
        Directory.SetCurrentDirectory(Path.Combine(Application.dataPath, "..\\"));

        // Free up space in cache.
        try
        {
            foreach (string f in Directory.GetFiles(HLib.path_cacheFolder_mods))
            {
                if (!f.EndsWith("_" + Application.version + ".dll"))
                {
                    try
                    {
                        File.Delete(f);
                    }
                    catch { }
                }
            }

            foreach (string f in Directory.GetFiles(HLib.path_cacheFolder_temp))
            {
                if (f.EndsWith("csc_preloadsample.cs")) continue;
                try
                {
                    File.Delete(f);
                }
                catch { }
            }
        }
        catch { }

        StartCoroutine(LoadRML());
    }

    [Serializable]
    private class ServerVersionInfo
    {
        public string clientVersion = "";
    }

    IEnumerator LoadRML()
    {
        Directory.GetFiles(HLib.path_cacheFolder_temp).Where(s => s.ToLower().EndsWith(".dll")).ToList().ForEach((s) => File.Delete(s));

        ComponentManager<RML_Main>.Value = this;
        var request = AssetBundle.LoadFromFileAsync(Path.Combine(HLib.path_dataFolder, "rml.assets"));
        yield return request;
        HLib.bundle = request.assetBundle;

        ComponentManager<RSocket>.Value = gameObject.AddComponent<RSocket>();

        HLib.missingTexture = HLib.bundle.LoadAsset<Sprite>("missing").texture;
        gameObject.AddComponent<RConsole>();
        while (!GameObject.Find("RConsole")) { yield return new WaitForEndOfFrame(); }

        try
        {
            var harmony = new Harmony("hytekgames.raftmodloader");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        ComponentManager<RawSharp>.Value = gameObject.AddComponent<RawSharp>();
        ComponentManager<SocketIOComponent>.Value = gameObject.AddComponent<SocketIOComponent>();
        GameObject mainThread = new GameObject("HMainThreadDispatcher");
        mainThread.transform.SetParent(gameObject.transform);
        ComponentManager<UnityMainThreadDispatcher>.Value = mainThread.AddComponent<UnityMainThreadDispatcher>();
        ComponentManager<RNetworkImprovements>.Value = gameObject.AddComponent<RNetworkImprovements>();
        gameObject.AddComponent<CustomLoadingScreen>();

        var MenuAssetLoadRequest = HLib.bundle.LoadAssetAsync<GameObject>("RMLMainMenu");
        yield return MenuAssetLoadRequest;

        GameObject tempmenuobj = MenuAssetLoadRequest.asset as GameObject;
        GameObject mainmenu = Instantiate(tempmenuobj, gameObject.transform);
        ComponentManager<MainMenu>.Value = mainmenu.AddComponent<MainMenu>();

        var HNotifyLoadRequest = HLib.bundle.LoadAssetAsync<GameObject>("RMLNotificationSystem");
        yield return HNotifyLoadRequest;

        GameObject hnotifytempobj = HNotifyLoadRequest.asset as GameObject;
        GameObject hnotify = Instantiate(hnotifytempobj, gameObject.transform);
        ComponentManager<HNotify>.Value = hnotify.AddComponent<HNotify>();
        gameObject.AddComponent<RChat>();

        gameObject.AddComponent<NetworkModManager>();
    }
}