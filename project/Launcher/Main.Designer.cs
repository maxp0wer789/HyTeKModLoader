﻿namespace Launcher
{
    partial class Main
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.windowTitle = new System.Windows.Forms.Label();
            this.discordLabel = new System.Windows.Forms.Label();
            this.settingsLabel = new System.Windows.Forms.Label();
            this.websiteLabel = new System.Windows.Forms.Label();
            this.bottomBar = new System.Windows.Forms.Panel();
            this.downloadInfo = new System.Windows.Forms.PictureBox();
            this.statusBtn = new System.Windows.Forms.PictureBox();
            this.downloadsBtn = new System.Windows.Forms.PictureBox();
            this.statusLabel = new System.Windows.Forms.Label();
            this.downloadsLabel = new System.Windows.Forms.Label();
            this.downloadBtn_hitbox = new System.Windows.Forms.PictureBox();
            this.discordBtn_hitbox = new System.Windows.Forms.PictureBox();
            this.websiteBtn_hitbox = new System.Windows.Forms.PictureBox();
            this.statusBtn_hitbox = new System.Windows.Forms.PictureBox();
            this.settingsBtn_hitbox = new System.Windows.Forms.PictureBox();
            this.welcomeLabel = new System.Windows.Forms.Label();
            this.updateStatus = new System.Windows.Forms.Label();
            this.downloadPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.downloadPanel_Topbar = new System.Windows.Forms.Panel();
            this.downloadsPanelTitle = new System.Windows.Forms.Label();
            this.downloadPanelCloseBtn = new System.Windows.Forms.PictureBox();
            this.downloadWindowTitle = new System.Windows.Forms.Label();
            this.coremodPanel = new System.Windows.Forms.Panel();
            this.coremodBar = new System.Windows.Forms.Panel();
            this.coremodBar_BG = new System.Windows.Forms.Panel();
            this.coremodPercentage = new System.Windows.Forms.Label();
            this.coremodLabel = new System.Windows.Forms.Label();
            this.assetsPanel = new System.Windows.Forms.Panel();
            this.assetsBar = new System.Windows.Forms.Panel();
            this.assetsBar_BG = new System.Windows.Forms.Panel();
            this.assetsPercentage = new System.Windows.Forms.Label();
            this.assetsLabel = new System.Windows.Forms.Label();
            this.noDownloadsLabel = new System.Windows.Forms.Label();
            this.statusPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.statusPanel_Topbar = new System.Windows.Forms.Panel();
            this.statusPanelTitle = new System.Windows.Forms.Label();
            this.statusPanelCloseBtn = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.websiteStatus_BG = new System.Windows.Forms.Panel();
            this.websiteStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.statusWebsiteLabel = new System.Windows.Forms.Label();
            this.fastdlStatus_BG = new System.Windows.Forms.Panel();
            this.fastdlStatus = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.statusFastDLLabel = new System.Windows.Forms.Label();
            this.masterServerStatus_BG = new System.Windows.Forms.Panel();
            this.masterStatus = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.statusRuntimeLabel = new System.Windows.Forms.Label();
            this.apiStatus_BG = new System.Windows.Forms.Panel();
            this.apiStatus = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.statusAPILabel = new System.Windows.Forms.Label();
            this.settingsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.settingsPanel_Topbar = new System.Windows.Forms.Panel();
            this.settingsPanelCloseBtn = new System.Windows.Forms.PictureBox();
            this.settingsPanelTitle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gamepath_BG = new System.Windows.Forms.Panel();
            this.gamePathBtn = new System.Windows.Forms.Button();
            this.gamePathLabel = new System.Windows.Forms.Label();
            this.gamePathTitle = new System.Windows.Forms.Label();
            this.cachemanagement_BG = new System.Windows.Forms.Panel();
            this.TexturesCacheClearBtn = new System.Windows.Forms.Button();
            this.texturescachelabel = new System.Windows.Forms.Label();
            this.ModsCacheClearBtn = new System.Windows.Forms.Button();
            this.modscachelabel = new System.Windows.Forms.Label();
            this.texturecache_BG = new System.Windows.Forms.Label();
            this.modscache_BG = new System.Windows.Forms.Label();
            this.cacheManagementTitle = new System.Windows.Forms.Label();
            this.other_BG = new System.Windows.Forms.Panel();
            this.checkBoxSplashScreen = new System.Windows.Forms.CheckBox();
            this.splashscreenlabel = new System.Windows.Forms.Label();
            this.skipSplashScreen_BG = new System.Windows.Forms.Label();
            this.uninstall = new System.Windows.Forms.Button();
            this.other_BG2 = new System.Windows.Forms.Label();
            this.automaticGameStart_BG = new System.Windows.Forms.Panel();
            this.startGameFromSteam = new System.Windows.Forms.CheckBox();
            this.StartGameFromSteamLabel = new System.Windows.Forms.Label();
            this.automaticGameStartBackground = new System.Windows.Forms.Label();
            this.automaticGameStart_BG2 = new System.Windows.Forms.Label();
            this.useOldInjectionMethodBG = new System.Windows.Forms.Panel();
            this.useOldInjectionMethod = new System.Windows.Forms.CheckBox();
            this.useOldInjectionMethodLabel = new System.Windows.Forms.Label();
            this.useOldInjectionMethodBackground = new System.Windows.Forms.Label();
            this.useOldInjectionMethodBG2 = new System.Windows.Forms.Label();
            this.gameLogo = new System.Windows.Forms.PictureBox();
            this.closeBtn = new System.Windows.Forms.PictureBox();
            this.windowIcon = new System.Windows.Forms.PictureBox();
            this.playBtn = new System.Windows.Forms.Button();
            this.websiteBtn = new System.Windows.Forms.PictureBox();
            this.settingsBtn = new System.Windows.Forms.PictureBox();
            this.topbar = new System.Windows.Forms.PictureBox();
            this.discordBtn = new System.Windows.Forms.PictureBox();
            this.modsFolderIcon = new System.Windows.Forms.PictureBox();
            this.modsFolderLabel = new System.Windows.Forms.Label();
            this.modsFolderHitbox = new System.Windows.Forms.PictureBox();
            this.lineSpacer = new System.Windows.Forms.PictureBox();
            this.modCreatorLabel = new System.Windows.Forms.Label();
            this.modCreatorIcon = new System.Windows.Forms.PictureBox();
            this.modCreatorHitbox = new System.Windows.Forms.PictureBox();
            this.scifiBoxBottomRight = new System.Windows.Forms.PictureBox();
            this.triangleScifi2 = new System.Windows.Forms.PictureBox();
            this.triangleScifi3 = new System.Windows.Forms.PictureBox();
            this.triangleScifi1 = new System.Windows.Forms.PictureBox();
            this.closeBtn_Hitbox = new System.Windows.Forms.PictureBox();
            this.minimizeBtn = new System.Windows.Forms.PictureBox();
            this.minimizeBtnHitbox = new System.Windows.Forms.PictureBox();
            this.offlineMode = new System.Windows.Forms.Label();
            this.bottomBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downloadInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadBtn_hitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discordBtn_hitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.websiteBtn_hitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBtn_hitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBtn_hitbox)).BeginInit();
            this.downloadPanel.SuspendLayout();
            this.downloadPanel_Topbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downloadPanelCloseBtn)).BeginInit();
            this.coremodPanel.SuspendLayout();
            this.assetsPanel.SuspendLayout();
            this.statusPanel.SuspendLayout();
            this.statusPanel_Topbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusPanelCloseBtn)).BeginInit();
            this.websiteStatus_BG.SuspendLayout();
            this.fastdlStatus_BG.SuspendLayout();
            this.masterServerStatus_BG.SuspendLayout();
            this.apiStatus_BG.SuspendLayout();
            this.settingsPanel.SuspendLayout();
            this.settingsPanel_Topbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanelCloseBtn)).BeginInit();
            this.gamepath_BG.SuspendLayout();
            this.cachemanagement_BG.SuspendLayout();
            this.other_BG.SuspendLayout();
            this.automaticGameStart_BG.SuspendLayout();
            this.useOldInjectionMethodBG.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gameLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.websiteBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discordBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modsFolderIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modsFolderHitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineSpacer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modCreatorIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modCreatorHitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scifiBoxBottomRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleScifi2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleScifi3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleScifi1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeBtn_Hitbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeBtnHitbox)).BeginInit();
            this.SuspendLayout();
            // 
            // windowTitle
            // 
            this.windowTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.windowTitle, "windowTitle");
            this.windowTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.windowTitle.Name = "windowTitle";
            this.windowTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.windowTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.windowTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // discordLabel
            // 
            resources.ApplyResources(this.discordLabel, "discordLabel");
            this.discordLabel.ForeColor = System.Drawing.Color.White;
            this.discordLabel.Name = "discordLabel";
            this.discordLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_Discord);
            this.discordLabel.MouseEnter += new System.EventHandler(this.discordBtn_MouseEnter);
            this.discordLabel.MouseLeave += new System.EventHandler(this.discordBtn_MouseLeave);
            // 
            // settingsLabel
            // 
            resources.ApplyResources(this.settingsLabel, "settingsLabel");
            this.settingsLabel.ForeColor = System.Drawing.Color.White;
            this.settingsLabel.Name = "settingsLabel";
            this.settingsLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleSettingsStatus);
            this.settingsLabel.MouseEnter += new System.EventHandler(this.settingsBtn_MouseEnter);
            this.settingsLabel.MouseLeave += new System.EventHandler(this.settingsBtn_MouseLeave);
            // 
            // websiteLabel
            // 
            resources.ApplyResources(this.websiteLabel, "websiteLabel");
            this.websiteLabel.ForeColor = System.Drawing.Color.White;
            this.websiteLabel.Name = "websiteLabel";
            this.websiteLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_Website);
            this.websiteLabel.MouseEnter += new System.EventHandler(this.websiteBtn_MouseEnter);
            this.websiteLabel.MouseLeave += new System.EventHandler(this.websiteBtn_MouseLeave);
            // 
            // bottomBar
            // 
            resources.ApplyResources(this.bottomBar, "bottomBar");
            this.bottomBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.bottomBar.Controls.Add(this.downloadInfo);
            this.bottomBar.Controls.Add(this.statusBtn);
            this.bottomBar.Controls.Add(this.downloadsBtn);
            this.bottomBar.Controls.Add(this.websiteLabel);
            this.bottomBar.Controls.Add(this.statusLabel);
            this.bottomBar.Controls.Add(this.downloadsLabel);
            this.bottomBar.Controls.Add(this.settingsLabel);
            this.bottomBar.Controls.Add(this.discordLabel);
            this.bottomBar.Controls.Add(this.downloadBtn_hitbox);
            this.bottomBar.Controls.Add(this.discordBtn_hitbox);
            this.bottomBar.Controls.Add(this.websiteBtn_hitbox);
            this.bottomBar.Controls.Add(this.statusBtn_hitbox);
            this.bottomBar.Controls.Add(this.settingsBtn_hitbox);
            this.bottomBar.Name = "bottomBar";
            // 
            // downloadInfo
            // 
            this.downloadInfo.BackgroundImage = global::Launcher.Properties.Resources.greenhell_downloadPending;
            resources.ApplyResources(this.downloadInfo, "downloadInfo");
            this.downloadInfo.Name = "downloadInfo";
            this.downloadInfo.TabStop = false;
            this.downloadInfo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleDownloads);
            this.downloadInfo.MouseEnter += new System.EventHandler(this.downloadsBtn_MouseEnter);
            this.downloadInfo.MouseLeave += new System.EventHandler(this.downloadsBtn_MouseLeave);
            // 
            // statusBtn
            // 
            resources.ApplyResources(this.statusBtn, "statusBtn");
            this.statusBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.statusBtn.BackgroundImage = global::Launcher.Properties.Resources.ping;
            this.statusBtn.Name = "statusBtn";
            this.statusBtn.TabStop = false;
            this.statusBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleServicesStatus);
            this.statusBtn.MouseEnter += new System.EventHandler(this.statusBtn_MouseEnter);
            this.statusBtn.MouseLeave += new System.EventHandler(this.statusBtn_MouseLeave);
            // 
            // downloadsBtn
            // 
            this.downloadsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.downloadsBtn.BackgroundImage = global::Launcher.Properties.Resources.download;
            resources.ApplyResources(this.downloadsBtn, "downloadsBtn");
            this.downloadsBtn.Name = "downloadsBtn";
            this.downloadsBtn.TabStop = false;
            this.downloadsBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleDownloads);
            this.downloadsBtn.MouseEnter += new System.EventHandler(this.downloadsBtn_MouseEnter);
            this.downloadsBtn.MouseLeave += new System.EventHandler(this.downloadsBtn_MouseLeave);
            // 
            // statusLabel
            // 
            resources.ApplyResources(this.statusLabel, "statusLabel");
            this.statusLabel.ForeColor = System.Drawing.Color.White;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleServicesStatus);
            this.statusLabel.MouseEnter += new System.EventHandler(this.statusBtn_MouseEnter);
            this.statusLabel.MouseLeave += new System.EventHandler(this.statusBtn_MouseLeave);
            // 
            // downloadsLabel
            // 
            resources.ApplyResources(this.downloadsLabel, "downloadsLabel");
            this.downloadsLabel.ForeColor = System.Drawing.Color.White;
            this.downloadsLabel.Name = "downloadsLabel";
            this.downloadsLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleDownloads);
            this.downloadsLabel.MouseEnter += new System.EventHandler(this.downloadsBtn_MouseEnter);
            this.downloadsLabel.MouseLeave += new System.EventHandler(this.downloadsBtn_MouseLeave);
            // 
            // downloadBtn_hitbox
            // 
            this.downloadBtn_hitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.downloadBtn_hitbox, "downloadBtn_hitbox");
            this.downloadBtn_hitbox.Name = "downloadBtn_hitbox";
            this.downloadBtn_hitbox.TabStop = false;
            this.downloadBtn_hitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleDownloads);
            this.downloadBtn_hitbox.MouseEnter += new System.EventHandler(this.downloadsBtn_MouseEnter);
            this.downloadBtn_hitbox.MouseLeave += new System.EventHandler(this.downloadsBtn_MouseLeave);
            // 
            // discordBtn_hitbox
            // 
            this.discordBtn_hitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.discordBtn_hitbox, "discordBtn_hitbox");
            this.discordBtn_hitbox.Name = "discordBtn_hitbox";
            this.discordBtn_hitbox.TabStop = false;
            this.discordBtn_hitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_Discord);
            this.discordBtn_hitbox.MouseEnter += new System.EventHandler(this.discordBtn_MouseEnter);
            this.discordBtn_hitbox.MouseLeave += new System.EventHandler(this.discordBtn_MouseLeave);
            // 
            // websiteBtn_hitbox
            // 
            this.websiteBtn_hitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.websiteBtn_hitbox, "websiteBtn_hitbox");
            this.websiteBtn_hitbox.Name = "websiteBtn_hitbox";
            this.websiteBtn_hitbox.TabStop = false;
            this.websiteBtn_hitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_Website);
            this.websiteBtn_hitbox.MouseEnter += new System.EventHandler(this.websiteBtn_MouseEnter);
            this.websiteBtn_hitbox.MouseLeave += new System.EventHandler(this.websiteBtn_MouseLeave);
            // 
            // statusBtn_hitbox
            // 
            this.statusBtn_hitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.statusBtn_hitbox, "statusBtn_hitbox");
            this.statusBtn_hitbox.Name = "statusBtn_hitbox";
            this.statusBtn_hitbox.TabStop = false;
            this.statusBtn_hitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleServicesStatus);
            this.statusBtn_hitbox.MouseEnter += new System.EventHandler(this.statusBtn_MouseEnter);
            this.statusBtn_hitbox.MouseLeave += new System.EventHandler(this.statusBtn_MouseLeave);
            // 
            // settingsBtn_hitbox
            // 
            this.settingsBtn_hitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.settingsBtn_hitbox, "settingsBtn_hitbox");
            this.settingsBtn_hitbox.Name = "settingsBtn_hitbox";
            this.settingsBtn_hitbox.TabStop = false;
            this.settingsBtn_hitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleSettingsStatus);
            this.settingsBtn_hitbox.MouseEnter += new System.EventHandler(this.settingsBtn_MouseEnter);
            this.settingsBtn_hitbox.MouseLeave += new System.EventHandler(this.settingsBtn_MouseLeave);
            // 
            // welcomeLabel
            // 
            resources.ApplyResources(this.welcomeLabel, "welcomeLabel");
            this.welcomeLabel.ForeColor = System.Drawing.Color.White;
            this.welcomeLabel.Name = "welcomeLabel";
            // 
            // updateStatus
            // 
            resources.ApplyResources(this.updateStatus, "updateStatus");
            this.updateStatus.ForeColor = System.Drawing.Color.White;
            this.updateStatus.Name = "updateStatus";
            // 
            // downloadPanel
            // 
            this.downloadPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.downloadPanel.Controls.Add(this.downloadPanel_Topbar);
            this.downloadPanel.Controls.Add(this.coremodPanel);
            this.downloadPanel.Controls.Add(this.assetsPanel);
            this.downloadPanel.Controls.Add(this.noDownloadsLabel);
            resources.ApplyResources(this.downloadPanel, "downloadPanel");
            this.downloadPanel.Name = "downloadPanel";
            // 
            // downloadPanel_Topbar
            // 
            this.downloadPanel_Topbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.downloadPanel_Topbar.Controls.Add(this.downloadsPanelTitle);
            this.downloadPanel_Topbar.Controls.Add(this.downloadPanelCloseBtn);
            this.downloadPanel_Topbar.Controls.Add(this.downloadWindowTitle);
            resources.ApplyResources(this.downloadPanel_Topbar, "downloadPanel_Topbar");
            this.downloadPanel_Topbar.Name = "downloadPanel_Topbar";
            // 
            // downloadsPanelTitle
            // 
            this.downloadsPanelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            resources.ApplyResources(this.downloadsPanelTitle, "downloadsPanelTitle");
            this.downloadsPanelTitle.ForeColor = System.Drawing.Color.White;
            this.downloadsPanelTitle.Name = "downloadsPanelTitle";
            // 
            // downloadPanelCloseBtn
            // 
            this.downloadPanelCloseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.downloadPanelCloseBtn.BackgroundImage = global::Launcher.Properties.Resources.close;
            resources.ApplyResources(this.downloadPanelCloseBtn, "downloadPanelCloseBtn");
            this.downloadPanelCloseBtn.Name = "downloadPanelCloseBtn";
            this.downloadPanelCloseBtn.TabStop = false;
            this.downloadPanelCloseBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.downloadPanelCloseBtn_MouseClick);
            this.downloadPanelCloseBtn.MouseEnter += new System.EventHandler(this.downloadPanelCloseBtn_MouseEnter);
            this.downloadPanelCloseBtn.MouseLeave += new System.EventHandler(this.downloadPanelCloseBtn_MouseLeave);
            // 
            // downloadWindowTitle
            // 
            this.downloadWindowTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.downloadWindowTitle, "downloadWindowTitle");
            this.downloadWindowTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.downloadWindowTitle.Name = "downloadWindowTitle";
            // 
            // coremodPanel
            // 
            this.coremodPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.coremodPanel.Controls.Add(this.coremodBar);
            this.coremodPanel.Controls.Add(this.coremodBar_BG);
            this.coremodPanel.Controls.Add(this.coremodPercentage);
            this.coremodPanel.Controls.Add(this.coremodLabel);
            resources.ApplyResources(this.coremodPanel, "coremodPanel");
            this.coremodPanel.Name = "coremodPanel";
            // 
            // coremodBar
            // 
            this.coremodBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(29)))), ((int)(((byte)(35)))));
            resources.ApplyResources(this.coremodBar, "coremodBar");
            this.coremodBar.Name = "coremodBar";
            // 
            // coremodBar_BG
            // 
            this.coremodBar_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            resources.ApplyResources(this.coremodBar_BG, "coremodBar_BG");
            this.coremodBar_BG.Name = "coremodBar_BG";
            // 
            // coremodPercentage
            // 
            this.coremodPercentage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.coremodPercentage, "coremodPercentage");
            this.coremodPercentage.ForeColor = System.Drawing.Color.White;
            this.coremodPercentage.Name = "coremodPercentage";
            // 
            // coremodLabel
            // 
            this.coremodLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.coremodLabel, "coremodLabel");
            this.coremodLabel.ForeColor = System.Drawing.Color.White;
            this.coremodLabel.Name = "coremodLabel";
            // 
            // assetsPanel
            // 
            this.assetsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.assetsPanel.Controls.Add(this.assetsBar);
            this.assetsPanel.Controls.Add(this.assetsBar_BG);
            this.assetsPanel.Controls.Add(this.assetsPercentage);
            this.assetsPanel.Controls.Add(this.assetsLabel);
            resources.ApplyResources(this.assetsPanel, "assetsPanel");
            this.assetsPanel.Name = "assetsPanel";
            // 
            // assetsBar
            // 
            this.assetsBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(29)))), ((int)(((byte)(35)))));
            resources.ApplyResources(this.assetsBar, "assetsBar");
            this.assetsBar.Name = "assetsBar";
            // 
            // assetsBar_BG
            // 
            this.assetsBar_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            resources.ApplyResources(this.assetsBar_BG, "assetsBar_BG");
            this.assetsBar_BG.Name = "assetsBar_BG";
            // 
            // assetsPercentage
            // 
            this.assetsPercentage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.assetsPercentage, "assetsPercentage");
            this.assetsPercentage.ForeColor = System.Drawing.Color.White;
            this.assetsPercentage.Name = "assetsPercentage";
            // 
            // assetsLabel
            // 
            this.assetsLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.assetsLabel, "assetsLabel");
            this.assetsLabel.ForeColor = System.Drawing.Color.White;
            this.assetsLabel.Name = "assetsLabel";
            // 
            // noDownloadsLabel
            // 
            this.noDownloadsLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            resources.ApplyResources(this.noDownloadsLabel, "noDownloadsLabel");
            this.noDownloadsLabel.ForeColor = System.Drawing.Color.White;
            this.noDownloadsLabel.Name = "noDownloadsLabel";
            // 
            // statusPanel
            // 
            this.statusPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.statusPanel.Controls.Add(this.statusPanel_Topbar);
            this.statusPanel.Controls.Add(this.websiteStatus_BG);
            this.statusPanel.Controls.Add(this.fastdlStatus_BG);
            this.statusPanel.Controls.Add(this.masterServerStatus_BG);
            this.statusPanel.Controls.Add(this.apiStatus_BG);
            resources.ApplyResources(this.statusPanel, "statusPanel");
            this.statusPanel.Name = "statusPanel";
            // 
            // statusPanel_Topbar
            // 
            this.statusPanel_Topbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.statusPanel_Topbar.Controls.Add(this.statusPanelTitle);
            this.statusPanel_Topbar.Controls.Add(this.statusPanelCloseBtn);
            this.statusPanel_Topbar.Controls.Add(this.label3);
            resources.ApplyResources(this.statusPanel_Topbar, "statusPanel_Topbar");
            this.statusPanel_Topbar.Name = "statusPanel_Topbar";
            // 
            // statusPanelTitle
            // 
            this.statusPanelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            resources.ApplyResources(this.statusPanelTitle, "statusPanelTitle");
            this.statusPanelTitle.ForeColor = System.Drawing.Color.White;
            this.statusPanelTitle.Name = "statusPanelTitle";
            // 
            // statusPanelCloseBtn
            // 
            this.statusPanelCloseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.statusPanelCloseBtn.BackgroundImage = global::Launcher.Properties.Resources.close;
            resources.ApplyResources(this.statusPanelCloseBtn, "statusPanelCloseBtn");
            this.statusPanelCloseBtn.Name = "statusPanelCloseBtn";
            this.statusPanelCloseBtn.TabStop = false;
            this.statusPanelCloseBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.statusPanelCloseBtn_MouseClick);
            this.statusPanelCloseBtn.MouseEnter += new System.EventHandler(this.statusPanelCloseBtn_MouseEnter);
            this.statusPanelCloseBtn.MouseLeave += new System.EventHandler(this.statusPanelCloseBtn_MouseLeave);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label3.Name = "label3";
            // 
            // websiteStatus_BG
            // 
            this.websiteStatus_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.websiteStatus_BG.Controls.Add(this.websiteStatus);
            this.websiteStatus_BG.Controls.Add(this.label8);
            this.websiteStatus_BG.Controls.Add(this.statusWebsiteLabel);
            resources.ApplyResources(this.websiteStatus_BG, "websiteStatus_BG");
            this.websiteStatus_BG.Name = "websiteStatus_BG";
            // 
            // websiteStatus
            // 
            this.websiteStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.websiteStatus, "websiteStatus");
            this.websiteStatus.ForeColor = System.Drawing.Color.White;
            this.websiteStatus.Name = "websiteStatus";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label8.Name = "label8";
            // 
            // statusWebsiteLabel
            // 
            this.statusWebsiteLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.statusWebsiteLabel, "statusWebsiteLabel");
            this.statusWebsiteLabel.ForeColor = System.Drawing.Color.White;
            this.statusWebsiteLabel.Name = "statusWebsiteLabel";
            // 
            // fastdlStatus_BG
            // 
            this.fastdlStatus_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.fastdlStatus_BG.Controls.Add(this.fastdlStatus);
            this.fastdlStatus_BG.Controls.Add(this.label11);
            this.fastdlStatus_BG.Controls.Add(this.statusFastDLLabel);
            resources.ApplyResources(this.fastdlStatus_BG, "fastdlStatus_BG");
            this.fastdlStatus_BG.Name = "fastdlStatus_BG";
            // 
            // fastdlStatus
            // 
            this.fastdlStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.fastdlStatus, "fastdlStatus");
            this.fastdlStatus.ForeColor = System.Drawing.Color.White;
            this.fastdlStatus.Name = "fastdlStatus";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label11.Name = "label11";
            // 
            // statusFastDLLabel
            // 
            this.statusFastDLLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.statusFastDLLabel, "statusFastDLLabel");
            this.statusFastDLLabel.ForeColor = System.Drawing.Color.White;
            this.statusFastDLLabel.Name = "statusFastDLLabel";
            // 
            // masterServerStatus_BG
            // 
            this.masterServerStatus_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.masterServerStatus_BG.Controls.Add(this.masterStatus);
            this.masterServerStatus_BG.Controls.Add(this.label5);
            this.masterServerStatus_BG.Controls.Add(this.statusRuntimeLabel);
            resources.ApplyResources(this.masterServerStatus_BG, "masterServerStatus_BG");
            this.masterServerStatus_BG.Name = "masterServerStatus_BG";
            // 
            // masterStatus
            // 
            this.masterStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.masterStatus, "masterStatus");
            this.masterStatus.ForeColor = System.Drawing.Color.White;
            this.masterStatus.Name = "masterStatus";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label5.Name = "label5";
            // 
            // statusRuntimeLabel
            // 
            this.statusRuntimeLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.statusRuntimeLabel, "statusRuntimeLabel");
            this.statusRuntimeLabel.ForeColor = System.Drawing.Color.White;
            this.statusRuntimeLabel.Name = "statusRuntimeLabel";
            // 
            // apiStatus_BG
            // 
            this.apiStatus_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.apiStatus_BG.Controls.Add(this.apiStatus);
            this.apiStatus_BG.Controls.Add(this.label10);
            this.apiStatus_BG.Controls.Add(this.statusAPILabel);
            resources.ApplyResources(this.apiStatus_BG, "apiStatus_BG");
            this.apiStatus_BG.Name = "apiStatus_BG";
            // 
            // apiStatus
            // 
            this.apiStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.apiStatus, "apiStatus");
            this.apiStatus.ForeColor = System.Drawing.Color.White;
            this.apiStatus.Name = "apiStatus";
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label10.Name = "label10";
            // 
            // statusAPILabel
            // 
            this.statusAPILabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.statusAPILabel, "statusAPILabel");
            this.statusAPILabel.ForeColor = System.Drawing.Color.White;
            this.statusAPILabel.Name = "statusAPILabel";
            // 
            // settingsPanel
            // 
            this.settingsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.settingsPanel.Controls.Add(this.settingsPanel_Topbar);
            this.settingsPanel.Controls.Add(this.gamepath_BG);
            this.settingsPanel.Controls.Add(this.cachemanagement_BG);
            this.settingsPanel.Controls.Add(this.other_BG);
            this.settingsPanel.Controls.Add(this.automaticGameStart_BG);
            this.settingsPanel.Controls.Add(this.useOldInjectionMethodBG);
            resources.ApplyResources(this.settingsPanel, "settingsPanel");
            this.settingsPanel.Name = "settingsPanel";
            // 
            // settingsPanel_Topbar
            // 
            this.settingsPanel_Topbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.settingsPanel_Topbar.Controls.Add(this.settingsPanelCloseBtn);
            this.settingsPanel_Topbar.Controls.Add(this.settingsPanelTitle);
            this.settingsPanel_Topbar.Controls.Add(this.label2);
            resources.ApplyResources(this.settingsPanel_Topbar, "settingsPanel_Topbar");
            this.settingsPanel_Topbar.Name = "settingsPanel_Topbar";
            // 
            // settingsPanelCloseBtn
            // 
            this.settingsPanelCloseBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            this.settingsPanelCloseBtn.BackgroundImage = global::Launcher.Properties.Resources.close;
            resources.ApplyResources(this.settingsPanelCloseBtn, "settingsPanelCloseBtn");
            this.settingsPanelCloseBtn.Name = "settingsPanelCloseBtn";
            this.settingsPanelCloseBtn.TabStop = false;
            this.settingsPanelCloseBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.settingsPanelCloseBtn_MouseClick);
            this.settingsPanelCloseBtn.MouseEnter += new System.EventHandler(this.settingsPanelCloseBtn_MouseEnter);
            this.settingsPanelCloseBtn.MouseLeave += new System.EventHandler(this.settingsPanelCloseBtn_MouseLeave);
            // 
            // settingsPanelTitle
            // 
            this.settingsPanelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(42)))));
            resources.ApplyResources(this.settingsPanelTitle, "settingsPanelTitle");
            this.settingsPanelTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.settingsPanelTitle.Name = "settingsPanelTitle";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(90)))), ((int)(((byte)(100)))));
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.label2.Name = "label2";
            // 
            // gamepath_BG
            // 
            this.gamepath_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.gamepath_BG.Controls.Add(this.gamePathBtn);
            this.gamepath_BG.Controls.Add(this.gamePathLabel);
            this.gamepath_BG.Controls.Add(this.gamePathTitle);
            resources.ApplyResources(this.gamepath_BG, "gamepath_BG");
            this.gamepath_BG.Name = "gamepath_BG";
            // 
            // gamePathBtn
            // 
            resources.ApplyResources(this.gamePathBtn, "gamePathBtn");
            this.gamePathBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.gamePathBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.gamePathBtn.FlatAppearance.BorderSize = 0;
            this.gamePathBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.gamePathBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.gamePathBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.gamePathBtn.Name = "gamePathBtn";
            this.gamePathBtn.UseVisualStyleBackColor = false;
            this.gamePathBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gamePathBtn_MouseDown);
            // 
            // gamePathLabel
            // 
            resources.ApplyResources(this.gamePathLabel, "gamePathLabel");
            this.gamePathLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.gamePathLabel.ForeColor = System.Drawing.Color.White;
            this.gamePathLabel.Name = "gamePathLabel";
            // 
            // gamePathTitle
            // 
            this.gamePathTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.gamePathTitle, "gamePathTitle");
            this.gamePathTitle.ForeColor = System.Drawing.Color.White;
            this.gamePathTitle.Name = "gamePathTitle";
            // 
            // cachemanagement_BG
            // 
            this.cachemanagement_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.cachemanagement_BG.Controls.Add(this.TexturesCacheClearBtn);
            this.cachemanagement_BG.Controls.Add(this.texturescachelabel);
            this.cachemanagement_BG.Controls.Add(this.ModsCacheClearBtn);
            this.cachemanagement_BG.Controls.Add(this.modscachelabel);
            this.cachemanagement_BG.Controls.Add(this.texturecache_BG);
            this.cachemanagement_BG.Controls.Add(this.modscache_BG);
            this.cachemanagement_BG.Controls.Add(this.cacheManagementTitle);
            resources.ApplyResources(this.cachemanagement_BG, "cachemanagement_BG");
            this.cachemanagement_BG.Name = "cachemanagement_BG";
            // 
            // TexturesCacheClearBtn
            // 
            resources.ApplyResources(this.TexturesCacheClearBtn, "TexturesCacheClearBtn");
            this.TexturesCacheClearBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.TexturesCacheClearBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.TexturesCacheClearBtn.FlatAppearance.BorderSize = 0;
            this.TexturesCacheClearBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.TexturesCacheClearBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.TexturesCacheClearBtn.ForeColor = System.Drawing.Color.White;
            this.TexturesCacheClearBtn.Name = "TexturesCacheClearBtn";
            this.TexturesCacheClearBtn.UseVisualStyleBackColor = false;
            this.TexturesCacheClearBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TexturesCacheClearBtn_MouseDown);
            // 
            // texturescachelabel
            // 
            resources.ApplyResources(this.texturescachelabel, "texturescachelabel");
            this.texturescachelabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.texturescachelabel.ForeColor = System.Drawing.Color.White;
            this.texturescachelabel.Name = "texturescachelabel";
            // 
            // ModsCacheClearBtn
            // 
            resources.ApplyResources(this.ModsCacheClearBtn, "ModsCacheClearBtn");
            this.ModsCacheClearBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ModsCacheClearBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ModsCacheClearBtn.FlatAppearance.BorderSize = 0;
            this.ModsCacheClearBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ModsCacheClearBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ModsCacheClearBtn.ForeColor = System.Drawing.Color.White;
            this.ModsCacheClearBtn.Name = "ModsCacheClearBtn";
            this.ModsCacheClearBtn.UseVisualStyleBackColor = false;
            this.ModsCacheClearBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ModsCacheClearBtn_MouseDown);
            // 
            // modscachelabel
            // 
            resources.ApplyResources(this.modscachelabel, "modscachelabel");
            this.modscachelabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.modscachelabel.ForeColor = System.Drawing.Color.White;
            this.modscachelabel.Name = "modscachelabel";
            // 
            // texturecache_BG
            // 
            resources.ApplyResources(this.texturecache_BG, "texturecache_BG");
            this.texturecache_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.texturecache_BG.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.texturecache_BG.Name = "texturecache_BG";
            // 
            // modscache_BG
            // 
            resources.ApplyResources(this.modscache_BG, "modscache_BG");
            this.modscache_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.modscache_BG.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.modscache_BG.Name = "modscache_BG";
            // 
            // cacheManagementTitle
            // 
            this.cacheManagementTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            resources.ApplyResources(this.cacheManagementTitle, "cacheManagementTitle");
            this.cacheManagementTitle.ForeColor = System.Drawing.Color.White;
            this.cacheManagementTitle.Name = "cacheManagementTitle";
            // 
            // other_BG
            // 
            resources.ApplyResources(this.other_BG, "other_BG");
            this.other_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.other_BG.Controls.Add(this.checkBoxSplashScreen);
            this.other_BG.Controls.Add(this.splashscreenlabel);
            this.other_BG.Controls.Add(this.skipSplashScreen_BG);
            this.other_BG.Controls.Add(this.uninstall);
            this.other_BG.Controls.Add(this.other_BG2);
            this.other_BG.Name = "other_BG";
            // 
            // checkBoxSplashScreen
            // 
            this.checkBoxSplashScreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.checkBoxSplashScreen, "checkBoxSplashScreen");
            this.checkBoxSplashScreen.Name = "checkBoxSplashScreen";
            this.checkBoxSplashScreen.UseVisualStyleBackColor = false;
            this.checkBoxSplashScreen.CheckedChanged += new System.EventHandler(this.checkBoxSplashScreen_CheckedChanged);
            // 
            // splashscreenlabel
            // 
            this.splashscreenlabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.splashscreenlabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.splashscreenlabel, "splashscreenlabel");
            this.splashscreenlabel.ForeColor = System.Drawing.Color.White;
            this.splashscreenlabel.Name = "splashscreenlabel";
            // 
            // skipSplashScreen_BG
            // 
            this.skipSplashScreen_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.skipSplashScreen_BG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.skipSplashScreen_BG, "skipSplashScreen_BG");
            this.skipSplashScreen_BG.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.skipSplashScreen_BG.Name = "skipSplashScreen_BG";
            // 
            // uninstall
            // 
            this.uninstall.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.uninstall.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.uninstall.FlatAppearance.BorderSize = 0;
            this.uninstall.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.uninstall.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            resources.ApplyResources(this.uninstall, "uninstall");
            this.uninstall.ForeColor = System.Drawing.Color.White;
            this.uninstall.Name = "uninstall";
            this.uninstall.UseVisualStyleBackColor = false;
            this.uninstall.Click += new System.EventHandler(this.uninstall_Click);
            // 
            // other_BG2
            // 
            this.other_BG2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            resources.ApplyResources(this.other_BG2, "other_BG2");
            this.other_BG2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.other_BG2.Name = "other_BG2";
            // 
            // automaticGameStart_BG
            // 
            resources.ApplyResources(this.automaticGameStart_BG, "automaticGameStart_BG");
            this.automaticGameStart_BG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.automaticGameStart_BG.Controls.Add(this.startGameFromSteam);
            this.automaticGameStart_BG.Controls.Add(this.StartGameFromSteamLabel);
            this.automaticGameStart_BG.Controls.Add(this.automaticGameStartBackground);
            this.automaticGameStart_BG.Controls.Add(this.automaticGameStart_BG2);
            this.automaticGameStart_BG.Name = "automaticGameStart_BG";
            // 
            // startGameFromSteam
            // 
            this.startGameFromSteam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.startGameFromSteam, "startGameFromSteam");
            this.startGameFromSteam.Name = "startGameFromSteam";
            this.startGameFromSteam.UseVisualStyleBackColor = false;
            this.startGameFromSteam.CheckedChanged += new System.EventHandler(this.disableProcessStart_CheckedChanged);
            // 
            // StartGameFromSteamLabel
            // 
            this.StartGameFromSteamLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.StartGameFromSteamLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.StartGameFromSteamLabel, "StartGameFromSteamLabel");
            this.StartGameFromSteamLabel.ForeColor = System.Drawing.Color.White;
            this.StartGameFromSteamLabel.Name = "StartGameFromSteamLabel";
            // 
            // automaticGameStartBackground
            // 
            this.automaticGameStartBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.automaticGameStartBackground.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.automaticGameStartBackground, "automaticGameStartBackground");
            this.automaticGameStartBackground.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.automaticGameStartBackground.Name = "automaticGameStartBackground";
            // 
            // automaticGameStart_BG2
            // 
            this.automaticGameStart_BG2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            resources.ApplyResources(this.automaticGameStart_BG2, "automaticGameStart_BG2");
            this.automaticGameStart_BG2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.automaticGameStart_BG2.Name = "automaticGameStart_BG2";
            // 
            // useOldInjectionMethodBG
            // 
            resources.ApplyResources(this.useOldInjectionMethodBG, "useOldInjectionMethodBG");
            this.useOldInjectionMethodBG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.useOldInjectionMethodBG.Controls.Add(this.useOldInjectionMethod);
            this.useOldInjectionMethodBG.Controls.Add(this.useOldInjectionMethodLabel);
            this.useOldInjectionMethodBG.Controls.Add(this.useOldInjectionMethodBackground);
            this.useOldInjectionMethodBG.Controls.Add(this.useOldInjectionMethodBG2);
            this.useOldInjectionMethodBG.Name = "useOldInjectionMethodBG";
            // 
            // useOldInjectionMethod
            // 
            this.useOldInjectionMethod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.useOldInjectionMethod, "useOldInjectionMethod");
            this.useOldInjectionMethod.Name = "useOldInjectionMethod";
            this.useOldInjectionMethod.UseVisualStyleBackColor = false;
            this.useOldInjectionMethod.CheckedChanged += new System.EventHandler(this.useOldInjectionMethod_CheckedChanged);
            // 
            // useOldInjectionMethodLabel
            // 
            this.useOldInjectionMethodLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.useOldInjectionMethodLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.useOldInjectionMethodLabel, "useOldInjectionMethodLabel");
            this.useOldInjectionMethodLabel.ForeColor = System.Drawing.Color.White;
            this.useOldInjectionMethodLabel.Name = "useOldInjectionMethodLabel";
            // 
            // useOldInjectionMethodBackground
            // 
            this.useOldInjectionMethodBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.useOldInjectionMethodBackground.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.useOldInjectionMethodBackground, "useOldInjectionMethodBackground");
            this.useOldInjectionMethodBackground.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.useOldInjectionMethodBackground.Name = "useOldInjectionMethodBackground";
            // 
            // useOldInjectionMethodBG2
            // 
            this.useOldInjectionMethodBG2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            resources.ApplyResources(this.useOldInjectionMethodBG2, "useOldInjectionMethodBG2");
            this.useOldInjectionMethodBG2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(216)))), ((int)(((byte)(220)))));
            this.useOldInjectionMethodBG2.Name = "useOldInjectionMethodBG2";
            // 
            // gameLogo
            // 
            this.gameLogo.BackgroundImage = global::Launcher.Properties.Resources.greenhell_logo;
            resources.ApplyResources(this.gameLogo, "gameLogo");
            this.gameLogo.Name = "gameLogo";
            this.gameLogo.TabStop = false;
            // 
            // closeBtn
            // 
            this.closeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.closeBtn.BackgroundImage = global::Launcher.Properties.Resources.closewindow;
            resources.ApplyResources(this.closeBtn, "closeBtn");
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.TabStop = false;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            this.closeBtn.MouseEnter += new System.EventHandler(this.closeBtn_MouseEnter);
            this.closeBtn.MouseLeave += new System.EventHandler(this.closeBtn_MouseLeave);
            // 
            // windowIcon
            // 
            this.windowIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.windowIcon, "windowIcon");
            this.windowIcon.Image = global::Launcher.Properties.Resources.greenhell_ghml_logo;
            this.windowIcon.Name = "windowIcon";
            this.windowIcon.TabStop = false;
            this.windowIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.windowIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.windowIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // playBtn
            // 
            this.playBtn.BackgroundImage = global::Launcher.Properties.Resources.greenhell_playBtn;
            resources.ApplyResources(this.playBtn, "playBtn");
            this.playBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.playBtn.FlatAppearance.BorderSize = 0;
            this.playBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.playBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.playBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.playBtn.Name = "playBtn";
            this.playBtn.UseVisualStyleBackColor = true;
            this.playBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.playBtn_MouseDown);
            this.playBtn.MouseEnter += new System.EventHandler(this.playBtn_MouseEnter);
            this.playBtn.MouseLeave += new System.EventHandler(this.playBtn_MouseLeave);
            // 
            // websiteBtn
            // 
            resources.ApplyResources(this.websiteBtn, "websiteBtn");
            this.websiteBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.websiteBtn.BackgroundImage = global::Launcher.Properties.Resources.website;
            this.websiteBtn.Name = "websiteBtn";
            this.websiteBtn.TabStop = false;
            this.websiteBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_Website);
            this.websiteBtn.MouseEnter += new System.EventHandler(this.websiteBtn_MouseEnter);
            this.websiteBtn.MouseLeave += new System.EventHandler(this.websiteBtn_MouseLeave);
            // 
            // settingsBtn
            // 
            resources.ApplyResources(this.settingsBtn, "settingsBtn");
            this.settingsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.settingsBtn.BackgroundImage = global::Launcher.Properties.Resources.settings;
            this.settingsBtn.Name = "settingsBtn";
            this.settingsBtn.TabStop = false;
            this.settingsBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_ToggleSettingsStatus);
            this.settingsBtn.MouseEnter += new System.EventHandler(this.settingsBtn_MouseEnter);
            this.settingsBtn.MouseLeave += new System.EventHandler(this.settingsBtn_MouseLeave);
            // 
            // topbar
            // 
            this.topbar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.topbar, "topbar");
            this.topbar.Name = "topbar";
            this.topbar.TabStop = false;
            this.topbar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragMouseDown);
            this.topbar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DragMouseMove);
            this.topbar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DragMouseUp);
            // 
            // discordBtn
            // 
            resources.ApplyResources(this.discordBtn, "discordBtn");
            this.discordBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.discordBtn.Name = "discordBtn";
            this.discordBtn.TabStop = false;
            this.discordBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnClick_Discord);
            this.discordBtn.MouseEnter += new System.EventHandler(this.discordBtn_MouseEnter);
            this.discordBtn.MouseLeave += new System.EventHandler(this.discordBtn_MouseLeave);
            // 
            // modsFolderIcon
            // 
            resources.ApplyResources(this.modsFolderIcon, "modsFolderIcon");
            this.modsFolderIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.modsFolderIcon.BackgroundImage = global::Launcher.Properties.Resources.folder;
            this.modsFolderIcon.Name = "modsFolderIcon";
            this.modsFolderIcon.TabStop = false;
            this.modsFolderIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpenModsFolder);
            this.modsFolderIcon.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.modsFolderIcon.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // modsFolderLabel
            // 
            resources.ApplyResources(this.modsFolderLabel, "modsFolderLabel");
            this.modsFolderLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.modsFolderLabel.ForeColor = System.Drawing.Color.White;
            this.modsFolderLabel.Name = "modsFolderLabel";
            this.modsFolderLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpenModsFolder);
            this.modsFolderLabel.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.modsFolderLabel.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // modsFolderHitbox
            // 
            resources.ApplyResources(this.modsFolderHitbox, "modsFolderHitbox");
            this.modsFolderHitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.modsFolderHitbox.Name = "modsFolderHitbox";
            this.modsFolderHitbox.TabStop = false;
            this.modsFolderHitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpenModsFolder);
            this.modsFolderHitbox.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.modsFolderHitbox.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // lineSpacer
            // 
            this.lineSpacer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.lineSpacer, "lineSpacer");
            this.lineSpacer.Name = "lineSpacer";
            this.lineSpacer.TabStop = false;
            // 
            // modCreatorLabel
            // 
            resources.ApplyResources(this.modCreatorLabel, "modCreatorLabel");
            this.modCreatorLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.modCreatorLabel.ForeColor = System.Drawing.Color.White;
            this.modCreatorLabel.Name = "modCreatorLabel";
            this.modCreatorLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpenModCreator);
            this.modCreatorLabel.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.modCreatorLabel.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            // 
            // modCreatorIcon
            // 
            resources.ApplyResources(this.modCreatorIcon, "modCreatorIcon");
            this.modCreatorIcon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.modCreatorIcon.BackgroundImage = global::Launcher.Properties.Resources.mod;
            this.modCreatorIcon.Name = "modCreatorIcon";
            this.modCreatorIcon.TabStop = false;
            this.modCreatorIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpenModCreator);
            this.modCreatorIcon.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.modCreatorIcon.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            // 
            // modCreatorHitbox
            // 
            resources.ApplyResources(this.modCreatorHitbox, "modCreatorHitbox");
            this.modCreatorHitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.modCreatorHitbox.Name = "modCreatorHitbox";
            this.modCreatorHitbox.TabStop = false;
            this.modCreatorHitbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OpenModCreator);
            this.modCreatorHitbox.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.modCreatorHitbox.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            // 
            // scifiBoxBottomRight
            // 
            resources.ApplyResources(this.scifiBoxBottomRight, "scifiBoxBottomRight");
            this.scifiBoxBottomRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.scifiBoxBottomRight.Name = "scifiBoxBottomRight";
            this.scifiBoxBottomRight.TabStop = false;
            // 
            // triangleScifi2
            // 
            resources.ApplyResources(this.triangleScifi2, "triangleScifi2");
            this.triangleScifi2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.triangleScifi2.BackgroundImage = global::Launcher.Properties.Resources.greenhell_triangle;
            this.triangleScifi2.Name = "triangleScifi2";
            this.triangleScifi2.TabStop = false;
            // 
            // triangleScifi3
            // 
            resources.ApplyResources(this.triangleScifi3, "triangleScifi3");
            this.triangleScifi3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.triangleScifi3.BackgroundImage = global::Launcher.Properties.Resources.greenhell_triangle;
            this.triangleScifi3.Name = "triangleScifi3";
            this.triangleScifi3.TabStop = false;
            // 
            // triangleScifi1
            // 
            resources.ApplyResources(this.triangleScifi1, "triangleScifi1");
            this.triangleScifi1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.triangleScifi1.BackgroundImage = global::Launcher.Properties.Resources.greenhell_triangle;
            this.triangleScifi1.Name = "triangleScifi1";
            this.triangleScifi1.TabStop = false;
            // 
            // closeBtn_Hitbox
            // 
            this.closeBtn_Hitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.closeBtn_Hitbox, "closeBtn_Hitbox");
            this.closeBtn_Hitbox.Name = "closeBtn_Hitbox";
            this.closeBtn_Hitbox.TabStop = false;
            this.closeBtn_Hitbox.Click += new System.EventHandler(this.closeBtn_Click);
            this.closeBtn_Hitbox.MouseEnter += new System.EventHandler(this.closeBtn_MouseEnter);
            this.closeBtn_Hitbox.MouseLeave += new System.EventHandler(this.closeBtn_MouseLeave);
            // 
            // minimizeBtn
            // 
            this.minimizeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.minimizeBtn.BackgroundImage = global::Launcher.Properties.Resources.minimize;
            resources.ApplyResources(this.minimizeBtn, "minimizeBtn");
            this.minimizeBtn.Name = "minimizeBtn";
            this.minimizeBtn.TabStop = false;
            this.minimizeBtn.Click += new System.EventHandler(this.minimizeBtn_Click);
            this.minimizeBtn.MouseEnter += new System.EventHandler(this.minimizeBtn_MouseEnter);
            this.minimizeBtn.MouseLeave += new System.EventHandler(this.minimizeBtn_MouseLeave);
            // 
            // minimizeBtnHitbox
            // 
            this.minimizeBtnHitbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            resources.ApplyResources(this.minimizeBtnHitbox, "minimizeBtnHitbox");
            this.minimizeBtnHitbox.Name = "minimizeBtnHitbox";
            this.minimizeBtnHitbox.TabStop = false;
            this.minimizeBtnHitbox.Click += new System.EventHandler(this.minimizeBtn_Click);
            this.minimizeBtnHitbox.MouseEnter += new System.EventHandler(this.minimizeBtn_MouseEnter);
            this.minimizeBtnHitbox.MouseLeave += new System.EventHandler(this.minimizeBtn_MouseLeave);
            // 
            // offlineMode
            // 
            resources.ApplyResources(this.offlineMode, "offlineMode");
            this.offlineMode.ForeColor = System.Drawing.Color.Red;
            this.offlineMode.Name = "offlineMode";
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.Controls.Add(this.settingsPanel);
            this.Controls.Add(this.websiteBtn);
            this.Controls.Add(this.settingsBtn);
            this.Controls.Add(this.discordBtn);
            this.Controls.Add(this.minimizeBtn);
            this.Controls.Add(this.minimizeBtnHitbox);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.closeBtn_Hitbox);
            this.Controls.Add(this.statusPanel);
            this.Controls.Add(this.modCreatorLabel);
            this.Controls.Add(this.modCreatorIcon);
            this.Controls.Add(this.modCreatorHitbox);
            this.Controls.Add(this.modsFolderLabel);
            this.Controls.Add(this.modsFolderIcon);
            this.Controls.Add(this.modsFolderHitbox);
            this.Controls.Add(this.downloadPanel);
            this.Controls.Add(this.gameLogo);
            this.Controls.Add(this.windowIcon);
            this.Controls.Add(this.windowTitle);
            this.Controls.Add(this.playBtn);
            this.Controls.Add(this.updateStatus);
            this.Controls.Add(this.welcomeLabel);
            this.Controls.Add(this.bottomBar);
            this.Controls.Add(this.topbar);
            this.Controls.Add(this.lineSpacer);
            this.Controls.Add(this.triangleScifi1);
            this.Controls.Add(this.triangleScifi3);
            this.Controls.Add(this.triangleScifi2);
            this.Controls.Add(this.scifiBoxBottomRight);
            this.Controls.Add(this.offlineMode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = global::Launcher.Properties.Resources.greenhell_ghml_icon;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.Opacity = 0.5D;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.bottomBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.downloadInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downloadBtn_hitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discordBtn_hitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.websiteBtn_hitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBtn_hitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBtn_hitbox)).EndInit();
            this.downloadPanel.ResumeLayout(false);
            this.downloadPanel_Topbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.downloadPanelCloseBtn)).EndInit();
            this.coremodPanel.ResumeLayout(false);
            this.assetsPanel.ResumeLayout(false);
            this.statusPanel.ResumeLayout(false);
            this.statusPanel_Topbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusPanelCloseBtn)).EndInit();
            this.websiteStatus_BG.ResumeLayout(false);
            this.fastdlStatus_BG.ResumeLayout(false);
            this.masterServerStatus_BG.ResumeLayout(false);
            this.apiStatus_BG.ResumeLayout(false);
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel_Topbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanelCloseBtn)).EndInit();
            this.gamepath_BG.ResumeLayout(false);
            this.cachemanagement_BG.ResumeLayout(false);
            this.other_BG.ResumeLayout(false);
            this.automaticGameStart_BG.ResumeLayout(false);
            this.useOldInjectionMethodBG.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gameLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.websiteBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discordBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modsFolderIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modsFolderHitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineSpacer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modCreatorIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modCreatorHitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scifiBoxBottomRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleScifi2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleScifi3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.triangleScifi1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeBtn_Hitbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minimizeBtnHitbox)).EndInit();
            this.ResumeLayout(false);

        }

#endregion
        public System.Windows.Forms.Label modscachelabel;
        public System.Windows.Forms.Label texturescachelabel;
        public System.Windows.Forms.PictureBox windowIcon;
        public System.Windows.Forms.Label windowTitle;
        public System.Windows.Forms.PictureBox closeBtn;
        public System.Windows.Forms.PictureBox settingsBtn;
        public System.Windows.Forms.PictureBox websiteBtn;
        public System.Windows.Forms.Label discordLabel;
        public System.Windows.Forms.Label settingsLabel;
        public System.Windows.Forms.Label websiteLabel;
        public System.Windows.Forms.Panel bottomBar;
        public System.Windows.Forms.PictureBox gameLogo;
        public System.Windows.Forms.Label welcomeLabel;
        public System.Windows.Forms.Button playBtn;
        public System.Windows.Forms.Label updateStatus;
        public System.Windows.Forms.PictureBox topbar;
        public System.Windows.Forms.FlowLayoutPanel downloadPanel;
        public System.Windows.Forms.Panel downloadPanel_Topbar;
        public System.Windows.Forms.Label downloadWindowTitle;
        public System.Windows.Forms.Panel coremodPanel;
        public System.Windows.Forms.Panel coremodBar;
        public System.Windows.Forms.Panel coremodBar_BG;
        public System.Windows.Forms.Label coremodPercentage;
        public System.Windows.Forms.Label coremodLabel;
        public System.Windows.Forms.Panel assetsPanel;
        public System.Windows.Forms.Panel assetsBar;
        public System.Windows.Forms.Panel assetsBar_BG;
        public System.Windows.Forms.Label assetsPercentage;
        public System.Windows.Forms.Label assetsLabel;
        public System.Windows.Forms.PictureBox downloadPanelCloseBtn;
        public System.Windows.Forms.Label downloadsPanelTitle;
        public System.Windows.Forms.Label downloadsLabel;
        public System.Windows.Forms.PictureBox downloadsBtn;
        public System.Windows.Forms.Label statusLabel;
        public System.Windows.Forms.PictureBox statusBtn;
        public System.Windows.Forms.PictureBox downloadInfo;
        public System.Windows.Forms.PictureBox discordBtn_hitbox;
        public System.Windows.Forms.PictureBox websiteBtn_hitbox;
        public System.Windows.Forms.PictureBox statusBtn_hitbox;
        public System.Windows.Forms.PictureBox settingsBtn_hitbox;
        public System.Windows.Forms.PictureBox downloadBtn_hitbox;
        public System.Windows.Forms.FlowLayoutPanel statusPanel;
        public System.Windows.Forms.Panel statusPanel_Topbar;
        public System.Windows.Forms.Label statusPanelTitle;
        public System.Windows.Forms.PictureBox statusPanelCloseBtn;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Panel websiteStatus_BG;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label websiteStatus;
        public System.Windows.Forms.Panel fastdlStatus_BG;
        public System.Windows.Forms.Label fastdlStatus;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label statusFastDLLabel;
        public System.Windows.Forms.Panel masterServerStatus_BG;
        public System.Windows.Forms.Label masterStatus;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label statusRuntimeLabel;
        public System.Windows.Forms.Label statusWebsiteLabel;
        public System.Windows.Forms.FlowLayoutPanel settingsPanel;
        public System.Windows.Forms.Panel settingsPanel_Topbar;
        public System.Windows.Forms.Label settingsPanelTitle;
        public System.Windows.Forms.PictureBox settingsPanelCloseBtn;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Panel gamepath_BG;
        public System.Windows.Forms.Button gamePathBtn;
        public System.Windows.Forms.Label gamePathLabel;
        public System.Windows.Forms.Label gamePathTitle;
        public System.Windows.Forms.Panel cachemanagement_BG;
        public System.Windows.Forms.Label cacheManagementTitle;
        public System.Windows.Forms.Button ModsCacheClearBtn;
        public System.Windows.Forms.Button TexturesCacheClearBtn;
        public System.Windows.Forms.Panel apiStatus_BG;
        public System.Windows.Forms.Label apiStatus;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label statusAPILabel;
        public System.Windows.Forms.PictureBox discordBtn;
        public System.Windows.Forms.PictureBox modsFolderIcon;
        public System.Windows.Forms.Label modsFolderLabel;
        public System.Windows.Forms.PictureBox modsFolderHitbox;
        public System.Windows.Forms.PictureBox lineSpacer;
        public System.Windows.Forms.Label modCreatorLabel;
        public System.Windows.Forms.PictureBox modCreatorIcon;
        public System.Windows.Forms.PictureBox modCreatorHitbox;
        public System.Windows.Forms.PictureBox scifiBoxBottomRight;
        public System.Windows.Forms.PictureBox triangleScifi2;
        public System.Windows.Forms.PictureBox triangleScifi3;
        public System.Windows.Forms.PictureBox triangleScifi1;
        public System.Windows.Forms.PictureBox closeBtn_Hitbox;
        public System.Windows.Forms.PictureBox minimizeBtn;
        public System.Windows.Forms.PictureBox minimizeBtnHitbox;
        public System.Windows.Forms.Panel other_BG;
        public System.Windows.Forms.Button uninstall;
        public System.Windows.Forms.Label noDownloadsLabel;
        public System.Windows.Forms.Label texturecache_BG;
        public System.Windows.Forms.Label modscache_BG;
        public System.Windows.Forms.Label other_BG2;
        public System.Windows.Forms.Label splashscreenlabel;
        public System.Windows.Forms.CheckBox checkBoxSplashScreen;
        public System.Windows.Forms.Label skipSplashScreen_BG;
        public System.Windows.Forms.Label offlineMode;
        public System.Windows.Forms.Panel automaticGameStart_BG;
        public System.Windows.Forms.CheckBox startGameFromSteam;
        public System.Windows.Forms.Label StartGameFromSteamLabel;
        public System.Windows.Forms.Label automaticGameStartBackground;
        public System.Windows.Forms.Label automaticGameStart_BG2;
        public System.Windows.Forms.Panel useOldInjectionMethodBG;
        public System.Windows.Forms.CheckBox useOldInjectionMethod;
        public System.Windows.Forms.Label useOldInjectionMethodLabel;
        public System.Windows.Forms.Label useOldInjectionMethodBackground;
        public System.Windows.Forms.Label useOldInjectionMethodBG2;
    }
}

