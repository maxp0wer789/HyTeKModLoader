﻿using System;
using System.Drawing;

[Serializable]
public class LauncherConfiguration
{
    public string gamePath = "";
    public string coreVersion = "?";
    public int agreeWithTOS = 0;
    public bool skipSplashScreen = false;
    public bool startGameFromSteam = false;
    public string branch = "public";
    public string gameStartCommand = "steam://run/" + STEAM_APPID;
    public bool useOldInjectionMethod = false;
#if GAME_IS_GREENHELL
    public static string STEAM_APPID = "815370";
    public static string GAME_NAME = "Green Hell";
    public static string GAME_MAINFILENAME = "GH.exe";
    public static string GAME_MAINFOLDERNAME = "Green Hell";
    public static string GAME_DATAFOLDERNAME = "GH_Data";
    public static string MODLOADER_NAME = "Green Hell Mod Loader";
    public static string MODLOADER_SITE = "greenhellmodding.com";
    public static ServiceInfo SERVICE_FASTDL = new ServiceInfo("fastdl.greenhellmodding.com", 80, false);
    public static ServiceInfo SERVICE_SITE = new ServiceInfo("greenhellmodding.com", 80, false);
    public static ServiceInfo SERVICE_MASTERSERVER = new ServiceInfo("62.210.119.159", 666, false);
    public static ServiceInfo SERVICE_DOCUMENTATION = new ServiceInfo("docs.greenhellmodding.com", 80, false);
    public static string MODLOADER_NAME_NOSPACES = "GreenHellModLoader";
    public static string MODLOADER_UPDATER_NAME = "GHMLLauncher.exe";
    public static string MODLOADER_NAME_SHORT = "GHML";
    public static string MODLOADER_DEVTEAM_NAME = "GreenHellModding";
    public static string MODLOADER_URISCHEME = "ghmllauncher";
    public static string DEDICATEDSERVER_MAINFILENAME = "GreenHellDedicatedServer.exe";
    public static string DISCORD_INVITEURL = "discord.gg/Qgdqmcy";
    public static string DISCORD_RICHPRESENCE_CLIENTID = "701568819025674300";
    public static string FILES_COREMOD = "coremod.dll";
    public static string FILES_ASSETS = "ghml.assets";
    public static string FOLDERS_DEVENV = "GreenHellModding";
    public static string MOD_EXTENSION = ".ghmod";
    public static Color THEME_HOVEREDLABEL_COLOR = Color.FromArgb(188, 32, 38);
    public static Color THEME_TOPBAR_COLOR = Color.FromArgb(37, 37, 37);
    public static Color THEME_TOPBAR_BUTTONS_HOVER_COLOR = Color.FromArgb(45, 45, 45);
#elif GAME_IS_RAFT
    public static string STEAM_APPID = "648800";
    public static string GAME_NAME = "Raft";
    public static string GAME_MAINFILENAME = "Raft.exe";
    public static string GAME_MAINFOLDERNAME = "Raft";
    public static string GAME_DATAFOLDERNAME = "Raft_Data";
    public static string MODLOADER_NAME = "Raft Mod Loader";
    public static string MODLOADER_SITE = "raftmodding.com";
    public static ServiceInfo SERVICE_FASTDL = new ServiceInfo("fastdl.raftmodding.com", 80, false);
    public static ServiceInfo SERVICE_SITE = new ServiceInfo("raftmodding.com", 80, false);
    public static ServiceInfo SERVICE_MASTERSERVER = new ServiceInfo("164.132.145.11", 8081, false);
    public static ServiceInfo SERVICE_DOCUMENTATION = new ServiceInfo("api.raftmodding.com", 80, false);
    public static string MODLOADER_NAME_NOSPACES = "RaftModLoader";
    public static string MODLOADER_UPDATER_NAME = "RMLLauncher.exe";
    public static string MODLOADER_NAME_SHORT = "RML";
    public static string MODLOADER_DEVTEAM_NAME = "RaftModding";
    public static string MODLOADER_URISCHEME = "rmllauncher";
    public static string DEDICATEDSERVER_MAINFILENAME = "RaftDedicatedServer.exe";
    public static string DISCORD_INVITEURL = "discord.gg/Dwz7GSA";
    public static string DISCORD_RICHPRESENCE_CLIENTID = "719332334238695457";
    public static string FILES_COREMOD = "coremod.dll";
    public static string FILES_ASSETS = "rml.assets";
    public static string FOLDERS_DEVENV = "RaftModding";
    public static string MOD_EXTENSION = ".rmod";
    public static Color THEME_BACKGROUND_COLOR = Color.FromArgb(55, 71, 79);
    public static Color THEME_TOPBAR_COLOR = Color.FromArgb(38, 50, 56);
    public static Color THEME_TOPBAR_BUTTONS_HOVER_COLOR = Color.FromArgb(28, 40, 46);
    public static Color THEME_SETTINGS_BUTTONS_HOVER_COLOR = Color.FromArgb(62, 80, 89);
    public static Color THEME_HOVEREDLABEL_COLOR = Color.FromArgb(28, 89, 105);
    public static Color THEME_LINE_COLOR = Color.FromArgb(84, 110, 122);
    public static Color THEME_PANELS_BACKGROUND_COLOR = Color.FromArgb(84, 110, 122);
    public static Color THEME_PANELS_TOPBAR_COLOR = Color.FromArgb(69, 90, 100);
    public static Color THEME_PROGRESSBAR_COLOR = Color.FromArgb(129, 199, 132);
#endif
}
