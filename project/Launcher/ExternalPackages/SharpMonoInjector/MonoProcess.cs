﻿using System;

namespace SharpMonoInjector
{
    public class MonoProcess
    {
        public IntPtr MonoModule { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }
    }
}
