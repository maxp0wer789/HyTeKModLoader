﻿using System;
using System.Drawing;
using System.Net.Sockets;
using System.Threading.Tasks;
using static LauncherConfiguration;

namespace Launcher
{
    public class ServicesTab : LauncherTab
    {
        public override void Initialize()
        {
            panel = Main.Get().statusPanel;
            panel.Location = new Point(801, 40);
        }

        public async override void OnTabOpen()
        {
            base.OnTabOpen();
            Main.SettingsTab.OnTabClose();
            Main.DownloadsTab.OnTabClose();
            GetServicesStatus();
            while (panel.Location.X > 474 && IsOpened)
            {
                await Task.Delay(1);
                panel.Location = new Point(Main.Clamp(panel.Location.X - 40, 474, 801), 40);
            }
        }

        public async override void OnTabClose()
        {
            base.OnTabClose();
            while (panel.Location.X < 801 && !IsOpened)
            {
                await Task.Delay(1);
                panel.Location = new Point(Main.Clamp(panel.Location.X + 40, 474, 801), 40);
            }
        }

        public static void GetServicesStatus()
        {
            Main window = Main.Get();
            window.websiteStatus.Text = "Checking...";
            window.websiteStatus.ForeColor = Color.White;
            window.fastdlStatus.Text = "Checking...";
            window.fastdlStatus.ForeColor = Color.White;
            window.masterStatus.Text = "Checking...";
            window.masterStatus.ForeColor = Color.White;
            window.apiStatus.Text = "Checking...";
            window.apiStatus.ForeColor = Color.White;

            var website = PingHost(SERVICE_SITE.ip, SERVICE_SITE.port).ContinueWith((task) =>
            {
                Main.Get().Invoke(new Action(() =>
                {
                    if (task.Result == -1)
                    {
                        window.websiteStatus.Text = "Offline " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.websiteStatus.ForeColor = Color.FromArgb(255, 255, 128, 128);
                    }
                    else if (task.Result < 200)
                    {
                        window.websiteStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.websiteStatus.ForeColor = Color.FromArgb(255, 192, 255, 192);
                    }
                    else
                    {
                        window.websiteStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.websiteStatus.ForeColor = Color.FromArgb(255, 255, 192, 128);
                    }
                }));
            });

            var fastdl = PingHost(SERVICE_FASTDL.ip, SERVICE_FASTDL.port).ContinueWith((task) =>
            {
                Main.Get().Invoke(new Action(() =>
                {
                    if (task.Result == -1)
                    {
                        window.fastdlStatus.Text = "Offline " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.fastdlStatus.ForeColor = Color.FromArgb(255, 255, 128, 128);
                    }
                    else if (task.Result < 200)
                    {
                        window.fastdlStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.fastdlStatus.ForeColor = Color.FromArgb(255, 192, 255, 192);
                    }
                    else
                    {
                        window.fastdlStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.fastdlStatus.ForeColor = Color.FromArgb(255, 255, 192, 128);
                    }
                }));
            });

            var master = PingHost(SERVICE_MASTERSERVER.ip, SERVICE_MASTERSERVER.port).ContinueWith((task) =>
            {
                Main.Get().Invoke(new Action(() =>
                {
                    if (task.Result == -1)
                    {
                        window.masterStatus.Text = "Offline " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.masterStatus.ForeColor = Color.FromArgb(255, 255, 128, 128);
                    }
                    else if (task.Result < 200)
                    {
                        window.masterStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.masterStatus.ForeColor = Color.FromArgb(255, 192, 255, 192);
                    }
                    else
                    {
                        window.masterStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.masterStatus.ForeColor = Color.FromArgb(255, 255, 192, 128);
                    }
                }));
            });

            var api = PingHost(SERVICE_DOCUMENTATION.ip, SERVICE_DOCUMENTATION.port).ContinueWith((task) =>
            {
                Main.Get().Invoke(new Action(() =>
                {
                    if (task.Result == -1)
                    {
                        window.apiStatus.Text = "Offline " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.apiStatus.ForeColor = Color.FromArgb(255, 255, 128, 128);
                    }
                    else if (task.Result < 200)
                    {
                        window.apiStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.apiStatus.ForeColor = Color.FromArgb(255, 192, 255, 192);
                    }
                    else
                    {
                        window.apiStatus.Text = "Online (" + task.Result + "ms) " + ((window.websiteStatus.Font.FontFamily.Name == "Arial") ? "" : "⚫️");
                        window.apiStatus.ForeColor = Color.FromArgb(255, 255, 192, 128);
                    }
                }));
            });
        }

        public async static Task<int> PingHost(string hostUri, int portNumber)
        {
            var client = new TcpClient();
            DateTime start = DateTime.Now;
            var connectTask = client.ConnectAsync(hostUri, portNumber);
            var waitTask = Task.Delay(5000);
            if (await Task.WhenAny(connectTask, waitTask) == connectTask)
            {
                if (connectTask.IsFaulted || connectTask.IsCanceled || connectTask.Exception != null)
                {
                    return -1;
                }
                DateTime end = DateTime.Now;
                return (end - start).Milliseconds;
            }
            return -1;
        }
    }
}
