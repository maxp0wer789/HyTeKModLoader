﻿using Gameloop.Vdf;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using static LauncherConfiguration;
using LauncherUpdater;
using IniParser.Model;
using IniParser;
using Newtonsoft.Json;
using System.Management;
using SharpMonoInjector;

namespace Launcher
{
    public partial class Main : Form
    {
        static Main instance;
        public static Main Get()
        {
            return instance;
        }

        public static LauncherTab DownloadsTab = new DownloadsTab();
        public static LauncherTab ServicesTab = new ServicesTab();
        public static LauncherTab SettingsTab = new SettingsTab();

        public static string FOLDER_MAIN = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES);
        public static string FOLDER_BINARIES = Path.Combine(FOLDER_MAIN, "binaries");
        public static string FOLDER_CACHE = Path.Combine(FOLDER_MAIN, "cache");
        public static string FOLDER_CACHE_MODS = Path.Combine(FOLDER_CACHE, "mods");
        public static string FOLDER_CACHE_TEXTURES = Path.Combine(FOLDER_CACHE, "textures");
        public static string FOLDER_CACHE_TEMPORARY = Path.Combine(FOLDER_CACHE, "temp");
        public static string CONFIGURATION_FILE = Path.Combine(FOLDER_MAIN, "config.ini");
        public static string PID_FILE = Path.Combine(FOLDER_MAIN, "pid.txt");
        public static string FILES_ASSEMBLYLOADER = Path.Combine(FOLDER_BINARIES, "HMLCoreLibrary.dll");
        static string LAUNCHER_EXECUTABLE = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES + "/HMLCore.exe");
        public static LauncherConfiguration CurrentConfig = new LauncherConfiguration();
        public string arguments = "";
        ModLoaderVersion latestUpdateInfo;

        public Main()
        {
            STARTUPINFO startInfo;
            GetStartupInfo(out startInfo);
            var startupPath = startInfo.lpTitle;

            if (startupPath.Contains("Microsoft\\Internet Explorer\\Quick Launch"))
            {
                MessageBox.Show("Oops, you did something wrong but don't worry, its easily fixable!\nOur software works with an application that keeps it up to date.\nYou should pin the " + MODLOADER_UPDATER_NAME + " file instead of this one.\n\nPinning this program means it won't correctly update mod loader when there's a new update and it will break your mod loader installation.\nThanks.\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - Oops, You did something wrong.", MessageBoxButtons.OK);
                Process.GetCurrentProcess().Kill();
            }

            instance = this;
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(ErrorHandler);
            Application.ThreadException += new ThreadExceptionEventHandler(ErrorHandler);

            RegisterUriScheme();
            RegisterFileExtensionsIcons();
            HandleArguments();

            arguments = string.Join(" ", Environment.GetCommandLineArgs().Skip(1));
            Opacity = 0;
            InitializeComponent();
            settingsPanel.AutoScroll = true;
            ThemeManager.InitializeTheme();
            ThemeManager.InitializeFonts();
            Size = new Size(800, 450);
            windowTitle.Text = "HyTeKModLoader Launcher " + Properties.Settings.Default["version"] + (new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator) ? " (Running as Administrator)" : "");
            noDownloadsLabel.Show();
            assetsPanel.Hide();
            coremodPanel.Hide();
            downloadInfo.Hide();
            assetsBar.Size = new Size(0, 25);
            coremodBar.Size = new Size(0, 25);
            //RunningChecker();
            DownloadsTab.Initialize();
            ServicesTab.Initialize();
            SettingsTab.Initialize();
        }

        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "GetStartupInfoA")]
        public static extern void GetStartupInfo(out STARTUPINFO lpStartupInfo);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct STARTUPINFO
        {
            public uint cb;
            public string lpReserved;
            public string lpDesktop;
            public string lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public ushort wShowWindow;
            public ushort cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        public void HandleArguments()
        {
            string[] args = Environment.GetCommandLineArgs().Skip(1).ToArray();
            if (string.Join(" ", args) == "-ignoresandboxie")
            {
                return;
            }
            args.ToList().ForEach(arg =>
            {
                if (arg.StartsWith(MODLOADER_URISCHEME + "://"))
                {
                    arg = arg.Replace(MODLOADER_URISCHEME + "://", "");
                    string[] input = arg.Split('/');
                    string command = input[0];
                    switch (command)
                    {
                        case "installmod":
                            if (input.Length == 2)
                            {
                                ModInstaller form = new ModInstaller(input[1], true);
                                form.ShowDialog();
                            }
                            break;
                    }
                    Process.GetCurrentProcess().Kill();
                }
                else
                {
                    if (File.Exists(arg) && arg.EndsWith(MOD_EXTENSION))
                    {
                        ModInstaller form = new ModInstaller(arg, false);
                        form.ShowDialog();
                    }
                    Process.GetCurrentProcess().Kill();
                }
            });
        }

#if GAME_IS_RAFT

        public Dictionary<string, string> audiofiles = new Dictionary<string, string>();
        // Fixed Hashes that won't trigger the audio warning since it should be enabled.
        public List<string> fixedAudioFiles = new List<string>()
        {
            // Update 12.01 
            "1f6c5360557b8cef0e7f9aaffec4fa34c85f71b404425bf30e705419a133ab090a69f3e844a1bcf0ef3bae55a815dbbd18d751dbdb4348d747952ff9b464fed7",
            // Update 12 pre release fixed
            "03464c4740d7d2b64b6581c8d57dde858f843aac13394209f33075b1788b8076779046802fd17bf8fc217b5c1a3f84e1bfc0e1a74324eb42d490dd93a38c6c2e",
            // Update 13
            "2e5235154da690e49cf56f7ecf918f534a2849c9b64673da312f498be0b153cf5c0c1886cba8874e0d40f49cbebdd4d67396c0c4dd8322ffd143a9e581716306",
            // Update 13.01 hotfix
            "056a9d620d97ad9b780d48d78545f4c53394db349cd93148b4e289872ba36109a40b2c9d2a871c73d937e905768b08ef4f3d5188471af53b7f0351504f03a63d",
            // Final chapter
            "0779411a2f766de41b2272c167c9587aa38b0e99a8f30d2298437741980f2fdb54dc7c7a6128ace29af781c75cbae67d8460904f1a300454683fb2e2e6224420",
            // Final Chapter Hotfix 1
            "ca9d205db53ab6a26fec9a0affdedb09188eec6987d23d9898b478e19dce70b66a52365f7bdbfeafaf9893f0de16436ca808f0b60f87ce5e4c98ecc74da69275",
            // Final Chapter Hotfix 2
            "f068575eb5fdd06da061c55d6d178078448b8be1a6b9bed7db706e5873257036b4fb754a42c47ff5a1bd37a520c77d9134b4cfaddf1cdc470236045262b34636",
            // Final Chapter Hotfix 3
            "a77ff6245fa25479398f3bbe05cc075970c759a439bc7b4fe012261ba23aedfc38fe21ae1ce67825702d5a3347d4cd62df7cde994c897e548e87c9ee5126a3cd",
            // Final Chapter Hotfix 4
            "066355fc77b1fa5d3e20f868a6acffb3386ad14f7dfdcfb5b6486acb76e2bf27d13fd1e3e96e392e02c30a74b847e73580c3769e1e91883c5b9bc3cfd0920a02",
            // Final Chapter Hotfix 5
            "6aa98bc0c265cdc4a41105065d6bee1faa689b56d5ef94ea8aa9d78e05b1a396a61c200a03ee0a2f8744a793d87c2100007050a69341cebfdbeff213b616fb69",
            // Final Chapter Hotfix 7 (no 6)
            "cb5eb549cbfb4469e488a1bee87b00017878995de849488012e5c2fc53c326a3c2531cca327b04c25284d9e79684790ad120805f30329ba522dd8eeea0cd7355",
            // Final Chapter Hotfix 8
            "5793324c8ac26763ed8b4066dbc3afc7c2968d63ae34524b5a818c285547070757db0a2b19c955a2793d2269382a915ddc3ccccaba096d48f42b5c65a201d6c9"

        };

        public void ChangeDisableUnityAudio(string path, bool newValue)
        {
            // audiofiles.Add("originalRaftFileHash", "newFileName");

            // Update 12.01
            audiofiles.Add("77474f8a9067048cd1d477430f0a4a6d3b16acd1dd49333eb49fbffadb2866301568838448ba0fa02c0602320786c4edd5f70f6ee4bcdb8e64a43a80b3832721", "Resources.Raft.audiofile_update1201.dat");
            // Update 12 (2019.3.5f1)
            audiofiles.Add("a21abc86cf284bf671b6f811a0984ecc664f8140b4da55febb99ea2131ad240e719f9b74b08101fe0f2233d0620826a55ec21d5d65703a985e0ffd9e115d4768", "Resources.Raft.audiofile_update12.dat");

            // Update 13
            audiofiles.Add("a58ac8dd8f7fec63836ca73c472885e9c74fec51bf299ea3def6b5cc745d12231bde13dbeab632bfb23e3cacb0224f61323a6a451da81552326b11bb10abc5f5", "Resources.Raft.audiofile_update13.dat");

            // Update 13.01 Hotfix
            audiofiles.Add("12530f535217cf53413f3ea12df38de792511e50657e0ea375af91522ed7c68f81aedb8f5e3ccdc12f4d4aec756f46754d1901cf6ab5345dd095d66a4fa15180", "Resources.Raft.audiofile_update1301.dat");

            // Final Chapter
            audiofiles.Add("bba2a7cb0aebe0e4b30a26bb0d8b91bedc992dcf03b09ab9ad0c8510639a0d29e44da808a64b032431046e387654381bdf683df19e760adbddd141b2f6f3b27a", "Resources.Raft.audiofile_finalchapter.dat");

            // Final Chapter Hotfix1
            audiofiles.Add("92b3d05db66a3bc129cfa1f4cc449fb7ff05a7fe729cc4bd1437d4d5c7901adc7378645a80443163d9431f76ff9620ebfa2451b7057a7ffa9214245fed275649", "Resources.Raft.audiofile_finalchapter_hotfix1.dat");

            // Final Chapter Hotfix2
            audiofiles.Add("9b188d263a589d3b5cd61242e0f52886ca62fc3593bb81f6443c16ba4001b67d1fdf05fc1fae40c1df8aee83ffb8e7c4dc8982a1c5fbe5ab2ee1702134ec3526", "Resources.Raft.audiofile_finalchapter_hotfix2.dat");

            // Final Chapter Hotfix3
            audiofiles.Add("514eeebb2cabd43147b4a2e73d7cf4865db4825ad1689f6b0d7c9b26e219917c3121ca65c35b339dbb5d949f5e614197bf99a8fbc537698f5665c436e5726b11", "Resources.Raft.audiofile_finalchapter_hotfix3.dat");

            // Final Chapter Hotfix4
            audiofiles.Add("e3c6bf1d1cfc07484b019af2d1799f98b4e3e7dbf7a2bf48a103d439e8ffcb66e5ccb78ebdb16d5d10745bd92a7d5d2aa60461b2c7a20c0892b854bfa650d37b", "Resources.Raft.audiofile_finalchapter_hotfix4.dat");

            // Final Chapter Hotfix5
            audiofiles.Add("f7c0c4f28bca2b89a74b67be508d2a1deaad0cf895300337a5a2c783abebc39fbe460b93b0c28512936cd93ca7a9a9ff15b004ea930820d17d635923b1589b35", "Resources.Raft.audiofile_finalchapter_hotfix5.dat");

            // Final Chapter Hotfix7 (no 6)
            audiofiles.Add("8375efdf260cb7e14d2aff70cae056704fbe8169c6547b0d08ad4082401eccd0db79da635d7fcf909a5ea36cfc2470ba6301879a9ba792a31700289c0cdd382a", "Resources.Raft.audiofile_finalchapter_hotfix7.dat");

            // Final Chapter Hotfix8 
            audiofiles.Add("d0e539fea2324cd6c1e2fab1cb6c5c3b5f7f0de88cf887bfafb5c8af223db70110a9bf685ed4c78c4566df21ff487ff583c8cc4da81feb887951bd3c15665018", "Resources.Raft.audiofile_finalchapter_hotfix8.dat");


            if (fixedAudioFiles.Contains(CalculateSHA512(path).ToLowerInvariant()))
            {
                return;
            }

            try
            {
                // file : Assembly.GetExecutingAssembly().GetManifestResourceStream("Launcher.Resources.cldb.dat")
                string hash = CalculateSHA512(path).ToLowerInvariant();
                if (audiofiles.ContainsKey(hash))
                {
                    File.WriteAllBytes(path, Program.GetEmbeddedFile(audiofiles[hash]));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                MessageBox.Show("An error occured while enabling the Raft built in audio system!\n\nAs we use a temporary method to enable custom audio, Please verify your game files using steam and it should work!\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
#endif

        public static void RegisterFileExtensionsIcons()
        {
            try
            {
#if GAME_IS_RAFT
                File.WriteAllBytes(Path.Combine(FOLDER_MAIN, "rmod_icon.ico"), Program.GetEmbeddedFile("Resources.rmod_icon.ico"));
                using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\.rmod"))
                {
                    using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
                    {
                        defaultIcon.SetValue("", Path.Combine(FOLDER_MAIN, "rmod_icon.ico"));
                    }
                    using (var commandKey = key.CreateSubKey(@"shell\open\command"))
                    {
                        commandKey.SetValue("", "\"" + LAUNCHER_EXECUTABLE + "\" \"%1\"");
                    }
                }
#elif GAME_IS_GREENHELL
                File.WriteAllBytes(Path.Combine(FOLDER_MAIN, "ghmod_icon.ico"), Program.GetEmbeddedFile("Resources.ghmod_icon.ico"));
                using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\.ghmod"))
                {
                    using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
                    {
                        defaultIcon.SetValue("", Path.Combine(FOLDER_MAIN, "ghmod_icon.ico"));
                    }
                    using (var commandKey = key.CreateSubKey(@"shell\open\command"))
                    {
                        commandKey.SetValue("", "\"" + LAUNCHER_EXECUTABLE + "\" \"%1\"");
                    }
                }
#endif


            }
            catch { }
        }

        public static void RegisterUriScheme()
        {
            try
            {
                using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\" + MODLOADER_URISCHEME))
                {
                    string applicationLocation = Application.ExecutablePath;
                    key.SetValue("", "URL:" + MODLOADER_NAME);
                    key.SetValue("URL Protocol", "");
                    using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
                    {
                        defaultIcon.SetValue("", LAUNCHER_EXECUTABLE + ",1");
                    }

                    using (var commandKey = key.CreateSubKey(@"shell\open\command"))
                    {
                        commandKey.SetValue("", "\"" + LAUNCHER_EXECUTABLE + "\" \"%1\"");
                    }
                }
            }
            catch { }
        }

        /*public async void RunningChecker()
        {
            do
            {
                if (!updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!", StringComparison.InvariantCulture) && !updateStatus.Text.StartsWith("An error occured while fetching for updates!", StringComparison.InvariantCulture))
                {
                    playBtn.Hide();
                }
                else
                {
                    playBtn.Show();
                    if (IsGameAlreadyRunning())
                    {
                        playBtn.Text = "Running";
                    }
                    else
                    {
                        if (updateStatus.Text == "An error occured while fetching for updates!")
                        {
                            playBtn.Text = "Play\nOffline";
                        }
                        else
                        {
                            playBtn.Text = "Play";
                        }
                    }
                }
                await Task.Delay(500);
            } while (true);
        }*/

        public void ErrorHandler(object sender, ThreadExceptionEventArgs args)
        {
            if (args.Exception.GetType() == typeof(OutOfMemoryException))
            {
                MessageBox.Show("Your system has run out of application memory.\n\nTo avoid problems with your computer, quit any applications you are not using.\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(DirectoryNotFoundException))
            {
                MessageBox.Show("A directory is missing!\n\n" + ErrorForm.FlattenException(args.Exception) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(Win32Exception))
            {
                MessageBox.Show(args.Exception.Message + "\n" + ErrorForm.FlattenException(args.Exception) + "\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(IOException))
            {
                MessageBox.Show("An unknown error occured!\n\n" + ErrorForm.FlattenException(args.Exception) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(UnauthorizedAccessException))
            {
                MessageBox.Show("An error occurred while trying to access a file/folder.\n\nSomething is preventing " + MODLOADER_NAME_NOSPACES + " from working properly.Please consider disabling your antivirus or whitelisting the app in its settings.Also make sure you have Administrator rights on your computer.\n\n" + args.Exception.Message + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                ErrorOccured(args.Exception);
                Application.Exit();
            }
        }

        public void ErrorHandler(object sender, UnhandledExceptionEventArgs args)
        {
            if (args.ExceptionObject.GetType() == typeof(TargetInvocationException))
            {
                TargetInvocationException exception = (TargetInvocationException)args.ExceptionObject;
                if (exception.InnerException != null && exception.InnerException.GetType() == typeof(UnauthorizedAccessException))
                {
                    MessageBox.Show("An error occurred while trying to access a file/folder.\n\nSomething is preventing " + MODLOADER_NAME_NOSPACES + " from working properly.Please consider disabling your antivirus or whitelisting the app in its settings.Also make sure you have Administrator rights on your computer.\n\n" + exception.InnerException.Message + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }

            ErrorOccured(args.ExceptionObject);
            Application.Exit();
        }

        public static bool isServerAvailable = false;
        private async void Form1_Load(object sender, EventArgs e)
        {
            var client = new TcpClient();
            var connectTask = client.ConnectAsync(SERVICE_FASTDL.ip, SERVICE_FASTDL.port);
            var waitTask = Task.Delay(15000);
            Exception error = new Exception(SERVICE_FASTDL.ip + ":" + SERVICE_FASTDL.port + " Timed Out!\n\nPLEASE NOTE, WE CAN NOT HELP YOU WITH THIS PROBLEM, It is probably related to your antivirus or windows defender, your firewall or your ISP (internet provider) in some extremely rare cases.");
            if (await Task.WhenAny(connectTask, waitTask) == connectTask)
            {
                if (connectTask.Exception != null)
                {
                    error = connectTask.Exception;
                }
                if (connectTask.IsFaulted || connectTask.IsCanceled || connectTask.Exception != null)
                {
                    isServerAvailable = false;
                }
                else
                {
                    isServerAvailable = true;
                }
            }
            if (!isServerAvailable)
            {
                if (error.InnerException != null)
                {
                    error = error.InnerException;
                }
                MessageBox.Show("A problem occured while connecting to the " + MODLOADER_DEVTEAM_NAME + " servers.\n\nPlease check your internet connection and try again\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                offlineMode.Show();
            }
            else
            {
                offlineMode.Hide();
            }
            await FadeIn(this, 5);
            CheckInstall();
        }
        bool isCoremodDownloaded = false;
        bool isAssetsDownloaded = false;

        async void DownloadModLoaderFiles()
        {
            isCoremodDownloaded = false;
            isAssetsDownloaded = false;
            if (latestUpdateInfo.coremod_url.EndsWith(".dll", StringComparison.InvariantCulture))
            {
                WebClient coremodDllClient = new WebClient();
                coremodDllClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(CoreModDllProgressChanged);
                coremodDllClient.DownloadFileCompleted += new AsyncCompletedEventHandler(CoreModDllDownloadCompleted);
                coremodDllClient.DownloadFileAsync(new Uri(latestUpdateInfo.coremod_url), Path.Combine(FOLDER_BINARIES, FILES_COREMOD));
            }

            if (latestUpdateInfo.assets_url.EndsWith(".assets", StringComparison.InvariantCulture))
            {
                WebClient assetsClient = new WebClient();
                assetsClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(AssetsProgressChanged);
                assetsClient.DownloadFileCompleted += new AsyncCompletedEventHandler(AssetsDownloadCompleted);
                assetsClient.DownloadFileAsync(new Uri(latestUpdateInfo.assets_url), Path.Combine(FOLDER_MAIN, FILES_ASSETS));
            }

            while (isCoremodDownloaded == false || isAssetsDownloaded == false)
            {
                await Task.Delay(1);
            }

            if (CalculateSHA512(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)).Equals(latestUpdateInfo.coremod_hash, StringComparison.InvariantCulture) && CalculateSHA512(Path.Combine(FOLDER_MAIN, FILES_ASSETS)).Equals(latestUpdateInfo.assets_hash, StringComparison.InvariantCulture))
            {
                CurrentConfig.coreVersion = latestUpdateInfo.version;
                UpdateConfigFile();
                updateStatus.Text = MODLOADER_NAME_NOSPACES + " is up to date! (v" + CurrentConfig.coreVersion + ")";
                playBtn.Show();
            }
            else
            {
                CurrentConfig.coreVersion = "?";
                updateStatus.Text = "An error occured while downloading the update!";
                MessageBox.Show("An error occured while downloading the update! The downloaded file has a different hash than the requested file!\n\nLocal " + FILES_COREMOD + " Hash = " + CalculateSHA512(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)) + "\nRequested " + FILES_COREMOD + " Hash = " + latestUpdateInfo.coremod_hash + "\n\nLocal " + FILES_ASSETS + " Hash = " + CalculateSHA512(Path.Combine(FOLDER_MAIN, FILES_ASSETS)) + "\nRequested " + FILES_ASSETS + " Hash = " + latestUpdateInfo.assets_hash + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                updateStatus.ForeColor = Color.IndianRed;
                UpdateConfigFile();
                return;
            }
        }

        private async void CoreModDllProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            noDownloadsLabel.Hide();
            downloadInfo.Show();
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            coremodPanel.Show();
            coremodPercentage.Text = Math.Round(percentage).ToString() + "%";
            float progress = (250f / 100f) * (float)Math.Round(percentage);
            coremodBar.Size = new Size((int)Math.Floor(progress), 25);
            await Task.Delay(0);
        }

        private async void CoreModDllDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            await UnblockModLoaderDirectoryFiles();
            isCoremodDownloaded = true;
            noDownloadsLabel.Show();
            downloadInfo.Hide();
            coremodPanel.Hide();
        }

        private async void AssetsProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            noDownloadsLabel.Hide();
            downloadInfo.Show();
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            assetsPanel.Show();
            assetsPercentage.Text = Math.Round(percentage).ToString() + "%";
            float progress = (250f / 100f) * (float)Math.Round(percentage);
            assetsBar.Size = new Size((int)Math.Floor(progress), 25);
            await Task.Delay(0);
        }

        private void AssetsDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            isAssetsDownloaded = true;
            noDownloadsLabel.Show();
            downloadInfo.Hide();
            assetsPanel.Hide();
        }

        async void CheckInstall()
        {
            try
            {
                try
                {
                    Directory.CreateDirectory(FOLDER_MAIN);
                    Directory.CreateDirectory(FOLDER_BINARIES);
                    Directory.CreateDirectory(FOLDER_CACHE);
                    Directory.CreateDirectory(FOLDER_CACHE_MODS);
                    Directory.CreateDirectory(FOLDER_CACHE_TEXTURES);
                    Directory.CreateDirectory(FOLDER_CACHE_TEMPORARY);
                }
                catch (Exception e)
                {
                    MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
                if (!File.Exists(CONFIGURATION_FILE))
                {
                    LauncherConfiguration newconfig = new LauncherConfiguration();
                    CurrentConfig = newconfig;
                    SaveLauncherConfiguration();
                }
                else
                {
                    try
                    {
                        CurrentConfig = GetLauncherConfiguration();
                    }
                    catch
                    {
                        File.Delete(CONFIGURATION_FILE);
                        CheckInstall();
                        return;
                    }
                }
                if (CurrentConfig.agreeWithTOS == 0)
                {
                    Form tosform = new TOSForm(this);
                    DialogResult result = tosform.ShowDialog();
                    if (result != DialogResult.Yes)
                    {
                        Application.Exit();
                    }
                }
                if (!IsGamePathValid(CurrentConfig.gamePath))
                {
                    await FindGameFolder();
                }
                try
                {
                    if (IsGamePathValid(CurrentConfig.gamePath))
                    {
                        if (!Directory.Exists(Path.Combine(CurrentConfig.gamePath, "mods")))
                        {
                            Directory.CreateDirectory(Path.Combine(CurrentConfig.gamePath, "mods"));
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                try
                {
                    string content = File.ReadAllText(Path.Combine(CurrentConfig.gamePath, "Raft_Data/boot.config")).Replace("single-instance=", "");
                    File.WriteAllText(Path.Combine(CurrentConfig.gamePath, "Raft_Data/boot.config"), content);
                }
                catch { }

                // Extract the AssemblyLoader so mods can access the new libraries location
                try
                {
                    File.WriteAllBytes(FILES_ASSEMBLYLOADER, Program.GetEmbeddedFile("Resources.AssemblyLoader.dll"));
                    if (!CurrentConfig.useOldInjectionMethod)
                        File.WriteAllBytes(Path.Combine(CurrentConfig.gamePath, MODLOADER_NAME_NOSPACES + ".dll"), Program.GetEmbeddedFile("Resources.AssemblyLoader.dll"));
                }
                catch (Exception e)
                {
                    MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\nCouldn't extract the file HMLCoreLibrary.dll to \"" + FILES_ASSEMBLYLOADER + "\"\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                UpdateConfigFile();
#if GAME_IS_RAFT
                ChangeDisableUnityAudio(Path.Combine(CurrentConfig.gamePath, @"Raft_Data\globalgamemanagers"), false);
#endif
                gamePathLabel.Text = CurrentConfig.gamePath;
#if GAME_IS_GREENHELL
                checkBoxSplashScreen.Checked = CurrentConfig.skipSplashScreen;
#endif
                startGameFromSteam.Checked = CurrentConfig.startGameFromSteam;
                useOldInjectionMethod.Checked = CurrentConfig.useOldInjectionMethod;
            }
            catch (Exception e)
            {
                MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease delete the " + MODLOADER_NAME_NOSPACES + " folder located at \"%appdata%/" + MODLOADER_NAME_NOSPACES + "\" and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            await CheckForFilesUpdates();
        }

        public bool IsGameAlreadyRunning()
        {
            List<Process> processes = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", "")).ToList();
            processes.AddRange(Process.GetProcessesByName(DEDICATEDSERVER_MAINFILENAME.Replace(".exe", "")).ToList());
            foreach (Process p in processes)
            {
                if (arguments.Contains("-ignoresandboxie"))
                {
                    if (GetProcessOwner(p.Id) == "AUTORITE NT\\ANONYMOUS LOGON") { continue; }
                }
                return true;
            }
            return false;
        }

        public string GetProcessOwner(int processId)
        {
            string query = "Select * From Win32_Process Where ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                string[] argList = new string[] { string.Empty, string.Empty };
                int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    // return DOMAIN\user
                    return argList[1] + "\\" + argList[0];
                }
            }

            return "NO OWNER";
        }

        private async void playBtn_MouseDown(object sender, MouseEventArgs e)
        {
            if (updateStatus.Text.StartsWith("An error occured while fetching for updates!"))
            {
                if (File.Exists(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)) && File.Exists(Path.Combine(FOLDER_MAIN, FILES_ASSETS)))
                {
                    MessageBox.Show("As an error occured while fetching for updates the ModLoader might not work correctly. \n\nIf it does not work, please check your internet connection and try again later.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Your ModLoader installation is missing important files and we couldn't download them! please check your internet connection and try again later.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
            else if (!updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!"))
            {
                return;
            }
            if (!CurrentConfig.useOldInjectionMethod)
            {
                updateStatus.Text = "Starting " + GAME_NAME + (CurrentConfig.startGameFromSteam ? " from steam" : " executable") + "...";
                try
                {
                    int doorstopResult = InstallDoorstop();
                    if (doorstopResult > 0)
                    {
                        await Task.Delay(100);
                        File.WriteAllText(PID_FILE, Process.GetCurrentProcess().Id.ToString());
                        await Task.Delay(500);
                        Process game;
                        if (CurrentConfig.startGameFromSteam)
                            game = Process.Start(CurrentConfig.gameStartCommand);
                        else
                            game = Process.Start(Path.Combine(CurrentConfig.gamePath, GAME_MAINFILENAME));
                        
                        DateTime now = DateTime.Now;
                        while (now.AddSeconds(2) >= DateTime.Now)
                        {
                            await Task.Delay(1);
                        }

                        if (game != null && !game.HasExited)
                        {
                            updateStatus.Text = "Game Successfully Started !";
                            await Task.Delay(5000);
                        }
                    }
                }
                catch (Exception err)
                {
                    updateStatus.Text = "An error occured when starting the game !";
                    MessageBox.Show("An error occured when starting the game !\nReason : " + err.Message + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                // OLD INJECTION SHIT, THIS SUCKS !
                UninstallDoorstop();






                Process[] processes = new Process[0];
                bool startedWithExe = false;
                if (IsGameAlreadyRunning()) { return; }
                try
                {
                    Process.Start(CurrentConfig.gameStartCommand);
                    playBtn.Text = "Starting...";
                    new Action(async () =>
                    {
                        await Task.Delay(5000);
                        playBtn.Text = "Play";
                    }).Invoke();
                }
                catch
                {
                    MessageBox.Show("Warning! We wasn't able to start the game using the start command \"" + CurrentConfig.gameStartCommand + "\", The game will start from the executable file, this is not supported and the game might crash!\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", "Warning! Failed to start game using Steam!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    try
                    {
                        Process game = Process.Start(Path.Combine(CurrentConfig.gamePath, GAME_MAINFILENAME));
                        processes = new Process[1] { game };
                        startedWithExe = true;
                        playBtn.Text = "Starting...";
                        new Action(async () =>
                        {
                            await Task.Delay(5000);
                            playBtn.Text = "Play";
                        }).Invoke();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                if (!startedWithExe)
                {
                    bool isReady = false;
                    int secondsRemaining = 15;
                    while (!isReady && secondsRemaining > 0)
                    {
                        if (arguments.Contains("-ignoresandboxie"))
                        {
                            processes = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", "")).Where(x => GetProcessOwner(x.Id) != "AUTORITE NT\\ANONYMOUS LOGON").ToArray();
                        }
                        else
                        {
                            processes = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                        }

                        if (processes.Length >= 1)
                        {
                            try
                            {
                                const ProcessAccessRights flags = ProcessAccessRights.PROCESS_ALL_ACCESS;
                                IntPtr handle;

                                if ((handle = Native.OpenProcess(flags, false, processes[0].Id)) != IntPtr.Zero)
                                {
                                    if (ProcessUtils.GetMonoModule(handle, out IntPtr mono))
                                    {
                                        isReady = true;
                                        updateStatus.Text = "Found " + GAME_NAME + " process!";
                                    }
                                    Native.CloseHandle(handle);
                                }
                            }
                            catch { }
                        }
                        updateStatus.Text = "Searching " + GAME_NAME + " process (" + secondsRemaining + "s)...";
                        await Task.Delay(1000);
                        secondsRemaining--;
                    }
                    updateStatus.Text = "Trying something....";
                    if (secondsRemaining <= 0)
                    {
                        updateStatus.Text = "Couldn't find " + GAME_NAME + " process. Attempt #2";
                        processes = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                        processes.ToList()/*.Where((_p) => GetProcessOwner(_p.Id) != "ANONYMOUS LOGON").ToList()*/.ForEach(pr => pr.Kill());
                        await Task.Delay(1000);
                        Process game = Process.Start(Path.Combine(CurrentConfig.gamePath, GAME_MAINFILENAME));
                        processes = new Process[1] { game };
                        try
                        {
                            const ProcessAccessRights flags = ProcessAccessRights.PROCESS_QUERY_INFORMATION | ProcessAccessRights.PROCESS_VM_READ;
                            IntPtr handle;

                            if ((handle = Native.OpenProcess(flags, false, processes[0].Id)) != IntPtr.Zero)
                            {
                                if (ProcessUtils.GetMonoModule(handle, out IntPtr mono))
                                {
                                    if (!processes[0].WaitForInputIdle(0))
                                    {
                                        isReady = true;
                                        updateStatus.Text = "Found " + GAME_NAME + " process!";
                                    }
                                }
                                Native.CloseHandle(handle);
                            }
                        }
                        catch { }
                    }
                }
                Process p = processes[0];
                int remainingTries = 10;
                string step = "inject";
                switch (step)
                {
                    case "inject":
                        if (!processes[0].HasExited)
                        {
                            updateStatus.Text = "Injecting...";
                            try
                            {
                                //await Task.Delay(20000);
                                new Injector(processes[0].Id).Inject(Program.GetEmbeddedFile("Resources.AssemblyLoader.dll"), "Doorstop", "Entrypoint", "StartInject");
                                updateStatus.Text = "Successfully Injected!";
                            }
                            catch (Exception injectexception)
                            {
                                if (remainingTries > 0)
                                {
                                    updateStatus.Text = "Injection Failed! Retrying... #" + remainingTries;
                                    MessageBox.Show(injectexception.StackTrace.ToString());
                                    await Task.Delay(1000);
                                    remainingTries--;
                                    goto case "inject";
                                }
                                updateStatus.Text = "An error occured during injection!";
                                throw injectexception;
                            }
                        }
                        break;
                }












            }
            await Task.Delay(2000);
            updateStatus.Text = MODLOADER_NAME_NOSPACES + " is up to date! (v" + CurrentConfig.coreVersion + ")";
            playBtn.Show();
        }

        public int InstallDoorstop()
        {
            if (IsGamePathValid(CurrentConfig.gamePath))
            {
                try
                {
                    byte[] config = Program.GetEmbeddedFile("Resources.Doorstop.doorstop_config.ini");
                    string configUtf8 = System.Text.Encoding.UTF8.GetString(config);
                    string finalConfig = configUtf8.Replace("_AssemblyPath_", MODLOADER_NAME_NOSPACES + ".dll");
                    File.WriteAllText(Path.Combine(CurrentConfig.gamePath, "doorstop_config.ini"), finalConfig);
                    File.WriteAllBytes(Path.Combine(CurrentConfig.gamePath, "winhttp.dll"), Program.GetEmbeddedFile("Resources.Doorstop.winhttp.dll"));
                    return 1;
                }
                catch (Exception ex)
                {
                    if (File.Exists(Path.Combine(CurrentConfig.gamePath, "winhttp.dll")) && File.Exists(Path.Combine(CurrentConfig.gamePath, "doorstop_config.ini"))) return 2;
                    updateStatus.Text = "Doorstop Extraction Failed !";
                    MessageBox.Show("An error occured when extracting Doorstop to the game directory !\nReason : " + ex.Message + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 0;
                }
            }
            return 0;
        }

        public void UninstallDoorstop()
        {
            if (IsGamePathValid(CurrentConfig.gamePath))
            {
                try
                {
                    if (File.Exists(Path.Combine(CurrentConfig.gamePath, "doorstop_config.ini")))
                        File.Delete(Path.Combine(CurrentConfig.gamePath, "doorstop_config.ini"));
                    if (File.Exists(Path.Combine(CurrentConfig.gamePath, "winhttp.dll")))
                        File.Delete(Path.Combine(CurrentConfig.gamePath, "winhttp.dll"));
                    if (File.Exists(Path.Combine(CurrentConfig.gamePath, MODLOADER_NAME_NOSPACES + ".dll")))
                        File.Delete(Path.Combine(CurrentConfig.gamePath, MODLOADER_NAME_NOSPACES + ".dll"));
                }
                catch { }
            }
        }

        async Task FindGameFolder()
        {
            updateStatus.Text = "Searching for your " + GAME_NAME + " installation...";
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\Valve\\Steam");
                string installPath = key.GetValue("InstallPath") as string;
                if (!File.Exists(Path.Combine(installPath, "config\\config.vdf")))
                {
                    key = Registry.LocalMachine.OpenSubKey("Software\\Valve\\Steam");
                    installPath = key.GetValue("InstallPath") as string;
                    if (!File.Exists(Path.Combine(installPath, "config\\config.vdf")))
                    {
                        installPath = @"C:\Program Files (x86)\Steam";
                    }
                }

                if (Directory.Exists(Path.Combine(installPath, "steamapps\\common\\" + GAME_MAINFOLDERNAME)))
                {
                    if (IsGamePathValid(Path.Combine(installPath, "steamapps\\common\\" + GAME_MAINFOLDERNAME)))
                    {
                        FoundGameFolder(Path.Combine(installPath, "steamapps\\common\\" + GAME_MAINFOLDERNAME));
                        return;
                    }
                }


                if (File.Exists(Path.Combine(installPath, "config\\config.vdf")))
                {
                    string potentialDirectory1 = "";
                    string potentialDirectory2 = "";
                    string potentialDirectory3 = "";
                    try
                    {
                        dynamic steamconfig = VdfConvert.Deserialize(File.ReadAllText(Path.Combine(installPath, "config\\config.vdf")));
                        if (steamconfig.Value["Software"]["Valve"]["Steam"].ContainsKey("BaseInstallFolder_1"))
                        {
                            potentialDirectory1 = Path.Combine(steamconfig.Value["Software"]["Valve"]["Steam"]["BaseInstallFolder_1"].ToString(), "steamapps\\common\\" + GAME_MAINFOLDERNAME);
                        }
                        if (steamconfig.Value["Software"]["Valve"]["Steam"].ContainsKey("BaseInstallFolder_2"))
                        {
                            potentialDirectory2 = Path.Combine(steamconfig.Value["Software"]["Valve"]["Steam"]["BaseInstallFolder_2"].ToString(), "steamapps\\common\\" + GAME_MAINFOLDERNAME);
                        }
                        if (steamconfig.Value["Software"]["Valve"]["Steam"].ContainsKey("BaseInstallFolder_3"))
                        {
                            potentialDirectory3 = Path.Combine(steamconfig.Value["Software"]["Valve"]["Steam"]["BaseInstallFolder_3"].ToString(), "steamapps\\common\\" + GAME_MAINFOLDERNAME);
                        }
                    }
                    catch { }

                    if (IsGamePathValid(potentialDirectory1))
                    {
                        FoundGameFolder(potentialDirectory1);
                        return;
                    }
                    if (IsGamePathValid(potentialDirectory2))
                    {
                        FoundGameFolder(potentialDirectory2);
                        return;
                    }
                    if (IsGamePathValid(potentialDirectory3))
                    {
                        FoundGameFolder(potentialDirectory3);
                        return;
                    }
                }
            }
            catch { }


            try
            {
                bool enableSteamMethod = false;

                DialogResult SteamDialogResult = MessageBox.Show("Do you want to use Steam to automatically find your " + GAME_NAME + " folder?", "Sorry, we weren't able to automatically find your " + GAME_NAME + " folder!", MessageBoxButtons.YesNo);
                if (SteamDialogResult == DialogResult.Yes)
                {
                    enableSteamMethod = true;
                }
                else
                {
                    enableSteamMethod = false;
                }

                if (enableSteamMethod)
                {
                    int remainingSeconds = 30;
                    updateStatus.Text = "Searching for your " + GAME_NAME + " installation (30s)...";
                    try
                    {
                        Process.Start("steam://run/" + STEAM_APPID);
                        Process[] gameProcesses = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                        do
                        {
                            gameProcesses = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                            await Task.Delay(999);
                            updateStatus.Text = "Searching for your " + GAME_NAME + " installation (" + remainingSeconds + "s)...";
                            remainingSeconds--;
                        } while (gameProcesses.Length == 0 && remainingSeconds > 0);

                        if (gameProcesses.Length > 0)
                        {
                            string temp = GetPathToApp(gameProcesses[0]);
                            foreach (Process p in gameProcesses)
                            {
                                p.Kill();
                            }
                            if (!string.IsNullOrWhiteSpace(temp) && !temp.ToLower().Contains("system32"))
                            {
                                if (IsGamePathValid(Path.GetDirectoryName(temp)))
                                {
                                    FoundGameFolder(Path.GetDirectoryName(temp));
                                    return;
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { }

            updateStatus.Text = "Please manually select your " + GAME_NAME + " folder.";
            int step = 0;
            switch (step)
            {
                case 0:
                    DialogResult dialogResult = MessageBox.Show("To install " + MODLOADER_NAME_NOSPACES + " please manually select your " + GAME_NAME + " folder by pressing \"OK\" to continue, If you want to abort press \"Cancel\".", "Sorry, we weren't able to automatically find your " + GAME_NAME + " folder!", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        step = 1;
                        goto case 1;
                    }
                    else
                    {
                        await CloseForm(this, 1);
                    }
                    break;
                case 1:

                    var dialog = new FolderSelectDialog
                    {
                        InitialDirectory = "",
                        Title = "Please Select Your " + GAME_NAME + " Folder"
                    };
                    if (dialog.Show(Handle))
                    {
                        string path = dialog.FileName;
                        if (IsGamePathValid(path))
                        {
                            FoundGameFolder(path);
                            return;
                        }
                        else
                        {
                            DialogResult dialogResult2 = MessageBox.Show(MODLOADER_NAME_NOSPACES + " is unable to locate " + GAME_MAINFILENAME + " in the selected folder. If you want to retry press OK, if you want to abort press cancel or close this window.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                            if (dialogResult2 == DialogResult.OK)
                            {
                                goto case 1;
                            }
                            else
                            {
                                await CloseForm(this, 1);
                            }
                        }
                    }
                    break;
            }

            await CloseForm(this, 1);
        }

        void ErrorOccured(object _e)
        {
            Icon = Properties.Resources.error;
            Form eform = new ErrorForm(_e);
            eform.Icon = Properties.Resources.error;
            eform.ShowDialog();
            Close();
            Application.Exit();
        }

        void FoundGameFolder(string path)
        {
            CurrentConfig.gamePath = path;
            UpdateConfigFile();
        }

        public static bool IsGamePathValid(string path)
        {
            if (path == null)
            {
                return false;
            }
            if (File.Exists(Path.Combine(path, GAME_MAINFILENAME)) && Directory.Exists(Path.Combine(path, GAME_DATAFOLDERNAME + "\\Managed")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void UpdateConfigFile()
        {
            try
            {
                SaveLauncherConfiguration();
            }
            catch (Exception e)
            {
                MessageBox.Show("A problem occured while updating the " + MODLOADER_NAME_NOSPACES + " configuration file.\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        [DllImport("Kernel32.dll")]
        static extern uint QueryFullProcessImageName(IntPtr hProcess, uint flags, StringBuilder text, out uint size);

        private string GetPathToApp(Process proc)
        {
            try
            {
                string pathToExe = string.Empty;

                if (null != proc)
                {
                    uint nChars = 256;
                    StringBuilder Buff = new StringBuilder((int)nChars);

                    uint success = QueryFullProcessImageName(proc.Handle, 0, Buff, out nChars);

                    if (0 != success)
                    {
                        pathToExe = Buff.ToString();
                    }
                    else
                    {
                        return "";
                    }
                }

                return pathToExe;
            }
            catch { }
            return "";
        }

        void ClearCache(bool modsCache)
        {
            DirectoryInfo di = new DirectoryInfo(modsCache ? FOLDER_CACHE_MODS : FOLDER_CACHE_TEXTURES);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
            Launcher.SettingsTab.UpdateCacheSize();
        }

        public async Task CheckForFilesUpdates()
        {
            try
            {
                updateStatus.Text = "Checking for updates...";
                Task<string> task = RequestData("https://" + SERVICE_FASTDL.ip + "/data/client.php?branch=" + CurrentConfig.branch);
                var waitTask = Task.Delay(15000);
                Exception error = new Exception("https://" + SERVICE_FASTDL.ip + "/data/client.php?branch=" + CurrentConfig.branch + " Timed Out!\n\nPLEASE NOTE, WE CAN NOT HELP YOU WITH THIS PROBLEM, It is probably related to your antivirus or windows defender, your firewall or your ISP (internet provider) in some extremely rare cases.");
                if (await Task.WhenAny(task, waitTask) == task)
                {
                    if (task.Result.Length > 10 && !task.IsFaulted && task.Exception == null && !task.IsCanceled && task.IsCompleted)
                    {
                        latestUpdateInfo = ReadModLoaderVersion(task.Result);
                        if (CurrentConfig.coreVersion != latestUpdateInfo.version || !File.Exists(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)) || !File.Exists(Path.Combine(FOLDER_MAIN, FILES_ASSETS)))
                        {
                            UpdateAvailable form = new UpdateAvailable(latestUpdateInfo);
                            DialogResult result = form.ShowDialog();
                            if (result == DialogResult.Yes)
                            {
                                updateStatus.Text = "Downloading latest update...";
                                DownloadModLoaderFiles();
                            }
                            else
                            {
                                Application.Exit();
                            }
                        }
                        else
                        {
                            updateStatus.Text = MODLOADER_NAME_NOSPACES + " is up to date! (v" + CurrentConfig.coreVersion + ")";
                            playBtn.Show();
                        }
                    }
                    else
                    {
                        if (task.Exception != null)
                        {
                            MessageBox.Show("An error occured while fetching for updates\n\n" + ErrorForm.FlattenException(task.Exception) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show("An error occured while fetching for updates\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("An error occured while fetching for updates\n\n" + ErrorForm.FlattenException(error) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while fetching for updates\n\n" + ErrorForm.FlattenException(ex) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                updateStatus.Text = "An error occured while fetching for updates!";
                playBtn.Show();
                playBtn.Text = "Play\nOffline";
            }
        }

        public static async Task<string> RequestData(string uri)
        {
            WebClient wc = new WebClient();
            wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            return await wc.DownloadStringTaskAsync(uri);
        }

        private async Task FadeIn(Form o, int interval = 80)
        {
            await Task.Delay(50);
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.07;
            }
            o.Opacity = 1;
        }

        public async void ReopenForm(int interval = 80)
        {
            var o = this;
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.07;
            }
            o.Opacity = 1;
        }

        public async void OpenAnotherForm(Form o, int interval = 80)
        {
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.07;
            }
            o.Opacity = 0;
        }

        private async Task CloseForm(Form o, int interval = 80)
        {
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.07;
            }
            o.Opacity = 0;
            Application.Exit();
        }

        private void btnClick_ToggleDownloads(object sender, MouseEventArgs e)
        {
            DownloadsTab.IsOpened = !DownloadsTab.IsOpened;
            if (DownloadsTab.IsOpened)
            {
                DownloadsTab.OnTabOpen();
            }
            else
            {
                DownloadsTab.OnTabClose();
            }
        }

        private void btnClick_ToggleServicesStatus(object sender, MouseEventArgs e)
        {
            ServicesTab.IsOpened = !ServicesTab.IsOpened;
            if (ServicesTab.IsOpened)
            {
                ServicesTab.OnTabOpen();
            }
            else
            {
                ServicesTab.OnTabClose();
            }
        }

        private void btnClick_ToggleSettingsStatus(object sender, MouseEventArgs e)
        {
            SettingsTab.IsOpened = !SettingsTab.IsOpened;
            if (SettingsTab.IsOpened)
            {
                SettingsTab.OnTabOpen();
            }
            else
            {
                SettingsTab.OnTabClose();
            }
        }

        private void btnClick_Website(object sender, MouseEventArgs e)
        {
            Process.Start("https://www." + MODLOADER_SITE);
        }

        private void btnClick_Discord(object sender, MouseEventArgs e)
        {
            Process.Start("https://" + DISCORD_INVITEURL);
        }

        private async void closeBtn_Click(object sender, EventArgs e)
        {
            await CloseForm(this, 1);
        }

        private void playBtn_MouseLeave(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            playBtn.BackgroundImage = Properties.Resources.greenhell_playBtn;
#elif GAME_IS_RAFT
            playBtn.BackgroundImage = Properties.Resources.raft_playBtn;
#endif
        }

        private void playBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            playBtn.BackgroundImage = Properties.Resources.greenhell_playBtn_hover;
#elif GAME_IS_RAFT
            playBtn.BackgroundImage = Properties.Resources.raft_playBtn_hover;
#endif
        }

        private void downloadPanelCloseBtn_MouseEnter(object sender, EventArgs e)
        {
            downloadPanelCloseBtn.Size = new Size(25, 25);
            downloadPanelCloseBtn.Location = new Point(294, 7);
        }

        private void downloadPanelCloseBtn_MouseLeave(object sender, EventArgs e)
        {
            downloadPanelCloseBtn.Size = new Size(20, 20);
            downloadPanelCloseBtn.Location = new Point(296, 10);
        }

        private void downloadPanelCloseBtn_MouseClick(object sender, MouseEventArgs e)
        {
            DownloadsTab.OnTabClose();
        }

        private void settingsPanelCloseBtn_MouseClick(object sender, MouseEventArgs e)
        {
            SettingsTab.OnTabClose();
        }

        private void statusPanelCloseBtn_MouseEnter(object sender, EventArgs e)
        {
            statusPanelCloseBtn.Size = new Size(25, 25);
            statusPanelCloseBtn.Location = new Point(7, 7);
        }

        private void statusPanelCloseBtn_MouseLeave(object sender, EventArgs e)
        {
            statusPanelCloseBtn.Size = new Size(20, 20);
            statusPanelCloseBtn.Location = new Point(10, 10);
        }

        private void statusPanelCloseBtn_MouseClick(object sender, MouseEventArgs e)
        {
            ServicesTab.OnTabClose();
        }

        bool mouseDown;
        int mouseX = 0, mouseY = 0;
        int mouseinX = 0, mouseinY = 0;

        private void DragMouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseinX = MousePosition.X - Bounds.X;
            mouseinY = MousePosition.Y - Bounds.Y;
        }

        private void DragMouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void downloadsBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            downloadsBtn.BackgroundImage = Properties.Resources.greenhell_download;
#elif GAME_IS_RAFT
            downloadsBtn.BackgroundImage = Properties.Resources.raft_download;
#endif
            downloadsLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void downloadsBtn_MouseLeave(object sender, EventArgs e)
        {
            downloadsBtn.BackgroundImage = Properties.Resources.download;
            downloadsLabel.ForeColor = Color.White;
        }

        private void websiteBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            websiteBtn.BackgroundImage = Properties.Resources.greenhell_website;
#elif GAME_IS_RAFT
            websiteBtn.BackgroundImage = Properties.Resources.raft_website;
#endif
            websiteLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void websiteBtn_MouseLeave(object sender, EventArgs e)
        {
            websiteBtn.BackgroundImage = Properties.Resources.website;
            websiteLabel.ForeColor = Color.White;
        }

        private void discordBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            discordBtn.BackgroundImage = Properties.Resources.greenhell_discord;
#elif GAME_IS_RAFT
            discordBtn.BackgroundImage = Properties.Resources.raft_discord;
#endif
            discordLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void discordBtn_MouseLeave(object sender, EventArgs e)
        {
            discordBtn.BackgroundImage = Properties.Resources.discord;
            discordLabel.ForeColor = Color.White;
        }

        private void statusBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            statusBtn.BackgroundImage = Properties.Resources.greenhell_ping;
#elif GAME_IS_RAFT
            statusBtn.BackgroundImage = Properties.Resources.raft_ping;
#endif
            statusLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void statusBtn_MouseLeave(object sender, EventArgs e)
        {
            statusBtn.BackgroundImage = Properties.Resources.ping;
            statusLabel.ForeColor = Color.White;
        }

        private void settingsBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            settingsBtn.BackgroundImage = Properties.Resources.greenhell_settings;
#elif GAME_IS_RAFT
            settingsBtn.BackgroundImage = Properties.Resources.raft_settings;
#endif
            settingsLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void settingsBtn_MouseLeave(object sender, EventArgs e)
        {
            settingsBtn.BackgroundImage = Properties.Resources.settings;
            settingsLabel.ForeColor = Color.White;
        }

        private void settingsPanelCloseBtn_MouseEnter(object sender, EventArgs e)
        {
            settingsPanelCloseBtn.Size = new Size(25, 25);
            settingsPanelCloseBtn.Location = new Point(7, 7);
        }

        private void settingsPanelCloseBtn_MouseLeave(object sender, EventArgs e)
        {
            settingsPanelCloseBtn.Size = new Size(20, 20);
            settingsPanelCloseBtn.Location = new Point(10, 10);
        }

        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - mouseinX;
                mouseY = MousePosition.Y - mouseinY;

                SetDesktopLocation(mouseX, mouseY);
            }
        }

        private void ModsCacheClearBtn_MouseDown(object sender, MouseEventArgs e)
        {
            ClearCache(true);
        }

        private void TexturesCacheClearBtn_MouseDown(object sender, MouseEventArgs e)
        {
            ClearCache(false);
        }

        private void gamePathBtn_MouseDown(object sender, MouseEventArgs e)
        {

            var dialog = new FolderSelectDialog
            {
                InitialDirectory = "",
                Title = "Please Select Your " + GAME_NAME + " Folder"
            };
            if (dialog.Show(Handle))
            {
                string path = dialog.FileName;
                if (File.Exists(Path.Combine(path, GAME_MAINFILENAME)))
                {
                    CurrentConfig.gamePath = path;
                    UpdateConfigFile();
                    return;
                }
                else
                {
                    MessageBox.Show(MODLOADER_NAME_NOSPACES + " is unable to locate " + GAME_MAINFILENAME + " in the selected folder. If you want to retry press OK, if you want to abort press cancel or close this window.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            modCreatorIcon.BackgroundImage = Properties.Resources.greenhell_modcreator;
#elif GAME_IS_RAFT
            modCreatorIcon.BackgroundImage = Properties.Resources.raft_modcreator;
#endif
            modCreatorLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            modCreatorIcon.BackgroundImage = Properties.Resources.mod;
            modCreatorLabel.ForeColor = Color.White;
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            modsFolderIcon.BackgroundImage = Properties.Resources.greenhell_folder;
#elif GAME_IS_RAFT
            modsFolderIcon.BackgroundImage = Properties.Resources.raft_folder;
#endif
            modsFolderLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            modsFolderIcon.BackgroundImage = Properties.Resources.folder;
            modsFolderLabel.ForeColor = Color.White;
        }

        private void OpenModsFolder(object sender, MouseEventArgs e)
        {
            if (Directory.Exists(CurrentConfig.gamePath))
            {
                Directory.CreateDirectory(Path.Combine(CurrentConfig.gamePath, "mods"));
                Directory.CreateDirectory(Path.Combine(CurrentConfig.gamePath, "mods/ModData"));

                if (updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!"))
                {
                    Process.Start(Path.Combine(CurrentConfig.gamePath, "mods"));
                }
            }
        }

        private void OpenModCreator(object sender, MouseEventArgs e)
        {
            if (updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!"))
            {
                OpenAnotherForm(this, 1);
                Form modcreator = new ModCreator(this);
                modcreator.ShowDialog();
            }
        }

        private void minimizeBtn_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void minimizeBtn_MouseEnter(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_BUTTONS_HOVER_COLOR;
            minimizeBtnHitbox.BackColor = color;
            minimizeBtn.BackColor = color;
        }

        private void minimizeBtn_MouseLeave(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_COLOR;
            minimizeBtnHitbox.BackColor = color;
            minimizeBtn.BackColor = color;
        }

        private void closeBtn_MouseEnter(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_BUTTONS_HOVER_COLOR;
            closeBtn_Hitbox.BackColor = color;
            closeBtn.BackColor = color;
        }

        private void closeBtn_MouseLeave(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_COLOR;
            closeBtn_Hitbox.BackColor = color;
            closeBtn.BackColor = color;
        }

        private void uninstall_Click(object sender, EventArgs e)
        {
            string remainingFiles = "";

            GC.Collect();
            GC.WaitForPendingFinalizers();

            bool _modsFolder = false;
            if (Directory.Exists(Path.Combine(CurrentConfig.gamePath, "mods")))
            {
                try
                {
                    Empty(new DirectoryInfo(Path.Combine(CurrentConfig.gamePath, "mods")));
                    _modsFolder = true;
                }
                catch { }
            }
            if (!_modsFolder)
            {
                remainingFiles += "Mods folder (" + Path.Combine(CurrentConfig.gamePath, "mods") + ")\n";
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            bool _dataFolder = false;
            if (Directory.Exists(FOLDER_MAIN))
            {
                try
                {
                    bool success = Empty(new DirectoryInfo(FOLDER_MAIN));
                    _dataFolder = true;
                }
                catch { }
            }
            if (!_dataFolder)
            {
                remainingFiles += MODLOADER_NAME_NOSPACES + " data folder (" + FOLDER_MAIN + ")\n";
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            bool _registry = false;
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"HyTeKGames\" + MODLOADER_NAME_NOSPACES);
                if (key != null)
                {
                    Registry.CurrentUser.DeleteSubKeyTree(@"HyTeKGames\" + MODLOADER_NAME_NOSPACES);
                }
                _registry = true;
            }
            catch { }
            if (!_registry)
            {
                remainingFiles += MODLOADER_NAME_NOSPACES + " registry values (HKEY_CURRENT_USER/HyTeKGames/" + MODLOADER_NAME_NOSPACES + ")\n";
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

#if GAME_IS_RAFT
            Process.Start("steam://validate/" + STEAM_APPID);
#endif

            if (remainingFiles == "")
            {
                MessageBox.Show(MODLOADER_NAME_NOSPACES + " has been successfully uninstalled!", MODLOADER_NAME_NOSPACES + " has been successfully uninstalled!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(MODLOADER_NAME_NOSPACES + " has been uninstalled, however there is still some remaining files that you need to manually delete!\nRemaining files and their location : \n\n" + remainingFiles + "\n\nAs " + MODLOADER_NAME_NOSPACES + " override some game files, steam needs to redownload 1 file. If the steam game verification doesn't automatically start, please manually verify your game files.", MODLOADER_NAME_NOSPACES + " has been successfully uninstalled!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            Application.Exit();
        }

        static string CalculateSHA512(string filename)
        {
            if (!File.Exists(filename))
            {
                return "FILE DOESN'T EXIST!";
            }
            using (var sha512 = SHA512.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = sha512.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToUpperInvariant();
                }
            }
        }

        static string CalculateSHA512FromBytes(byte[] bytes)
        {
            using (var sha512 = SHA512.Create())
            {
                using (var stream = new MemoryStream(bytes))
                {
                    var hash = sha512.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToUpperInvariant();
                }
            }
        }

        public static bool Empty(DirectoryInfo directory)
        {
            bool success = true;
            foreach (FileInfo file in directory.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch { success = false; }
            }
            foreach (DirectoryInfo subDirectory in directory.GetDirectories())
            {
                try
                {
                    subDirectory.Delete(true);
                }
                catch { success = false; }
            }
            return success;
        }

        /*public string GetProcessOwner(int processId)
        {
            try
            {
                string query = "Select * From Win32_Process Where ProcessID = " + processId;
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
                ManagementObjectCollection processList = searcher.Get();

                foreach (ManagementObject obj in processList)
                {
                    string[] argList = new string[] { string.Empty, string.Empty };
                    int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                    if (returnVal == 0)
                    {
                        return argList[0];
                    }
                }
            }
            catch { }

            return "NO OWNER";
        }*/

        private void disableProcessStart_CheckedChanged(object sender, EventArgs e)
        {
            CurrentConfig.startGameFromSteam = startGameFromSteam.Checked;
            UpdateConfigFile();
        }

        private void checkBoxSplashScreen_CheckedChanged(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            CurrentConfig.skipSplashScreen = checkBoxSplashScreen.Checked;
            UpdateConfigFile();
#endif
        }

        public static ModLoaderVersion ReadModLoaderVersion(string json)
        {
            ModLoaderVersion deserializedModLoaderVersion = new ModLoaderVersion();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedModLoaderVersion.GetType());
            deserializedModLoaderVersion = ser.ReadObject(ms) as ModLoaderVersion;
            ms.Close();
            return deserializedModLoaderVersion;
        }

        // I KNOW, I KNOW, TOO LAZY, A TOTALLY NEW LAUNCHER IS IN THE WORKS SO.. :D
        public static LauncherConfiguration GetLauncherConfiguration()
        {
            LauncherConfiguration config = new LauncherConfiguration();
            var parser = new FileIniDataParser();

            // This needs to be removed at some point.
            if (File.Exists(Path.Combine(FOLDER_BINARIES, "rml.assets")))
            {
                File.Delete(Path.Combine(FOLDER_BINARIES, "rml.assets"));
            }

            if (File.Exists(Path.Combine(FOLDER_BINARIES, "ghml.assets")))
            {
                File.Delete(Path.Combine(FOLDER_BINARIES, "ghml.assets"));
            }

            if (File.Exists(Path.Combine(FOLDER_MAIN, "launcher_config.json")))
            {
                try
                {
                    LauncherConfiguration deserializedLauncherConfiguration = JsonConvert.DeserializeObject<LauncherConfiguration>(File.ReadAllText(Path.Combine(FOLDER_MAIN, "launcher_config.json")));
                    IniData _data = new IniData();
                    typeof(LauncherConfiguration).GetFields().Where(f => !f.IsStatic).ToList().ForEach((field) =>
                    {
                        _data["launcher"][field.Name] = field.GetValue(deserializedLauncherConfiguration).ToString();
                    });
                    parser.WriteFile(CONFIGURATION_FILE, _data);
                }
                catch { }
                File.Delete(Path.Combine(FOLDER_MAIN, "launcher_config.json"));
            }

            IniData data = new IniData();
            try
            {
                data = parser.ReadFile(CONFIGURATION_FILE);
            }
            catch { }

            // I KNOW, I KNOW, TOO LAZY, A TOTALLY NEW LAUNCHER IS IN THE WORKS SO.. :D
            try { config.gamePath = data["launcher"]["gamePath"]; } catch { config.gamePath = ""; }
            try { config.coreVersion = data["launcher"]["coreVersion"]; } catch { config.coreVersion = "?"; }
            try { config.agreeWithTOS = int.Parse(data["launcher"]["agreeWithTOS"]); } catch { config.agreeWithTOS = 0; }
            try { config.skipSplashScreen = bool.Parse(data["launcher"]["skipSplashScreen"]); } catch { config.skipSplashScreen = false; }
            try { config.startGameFromSteam = bool.Parse(data["launcher"]["disableAutomaticGameStart"]); } catch { config.startGameFromSteam = false; }
            try { config.useOldInjectionMethod = bool.Parse(data["launcher"]["useOldInjectionMethod"]); } catch { config.useOldInjectionMethod = false; }
            try { config.branch = data["launcher"]["branch"]; } catch { config.branch = "public"; }

            return config;
        }

        private void useOldInjectionMethod_CheckedChanged(object sender, EventArgs e)
        {
            CurrentConfig.useOldInjectionMethod = useOldInjectionMethod.Checked;
            if (!CurrentConfig.useOldInjectionMethod)
            {
                try
                {
                    if (IsGamePathValid(CurrentConfig.gamePath))
                    {
                        File.WriteAllBytes(FILES_ASSEMBLYLOADER, Program.GetEmbeddedFile("Resources.AssemblyLoader.dll"));
                        File.WriteAllBytes(Path.Combine(CurrentConfig.gamePath, MODLOADER_NAME_NOSPACES + ".dll"), Program.GetEmbeddedFile("Resources.AssemblyLoader.dll"));
                    }
                }
                catch { }
            }
            else
            {
                try {
                    if (IsGamePathValid(CurrentConfig.gamePath))
                    {
                        if (File.Exists(Path.Combine(CurrentConfig.gamePath, "doorstop_config.ini")))
                            File.Delete(Path.Combine(CurrentConfig.gamePath, "doorstop_config.ini"));
                        if (File.Exists(Path.Combine(CurrentConfig.gamePath, "winhttp.dll")))
                            File.Delete(Path.Combine(CurrentConfig.gamePath, "winhttp.dll"));
                        if (File.Exists(Path.Combine(CurrentConfig.gamePath, MODLOADER_NAME_NOSPACES + ".dll")))
                            File.Delete(Path.Combine(CurrentConfig.gamePath, MODLOADER_NAME_NOSPACES + ".dll"));
                    }
                }
                catch { }
            }
            UpdateConfigFile();
        }

        public static void SaveLauncherConfiguration()
        {
            var parser = new FileIniDataParser();
            IniData data = new IniData();
            typeof(LauncherConfiguration).GetFields().Where(f => !f.IsStatic).ToList().ForEach((field) =>
            {
                data["launcher"][field.Name] = field.GetValue(CurrentConfig).ToString();
            });
            parser.WriteFile(CONFIGURATION_FILE, data);
        }

        public async Task UnblockModLoaderDirectoryFiles()
        {
            try
            {
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES + "/*");

                ProcessStartInfo startInfo = new ProcessStartInfo("Powershell.exe");
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = "dir -Recurse \"" + path + "\" | Unblock-File";

                Process.Start(startInfo);
                await Task.Delay(200);
            }
            catch { }
        }
    }
}

public class ModLoaderVersion
{
    public string version = "";
    public string coremod_url = "";
    public string assets_url = "";
    public string coremod_hash = "";
    public string assets_hash = "";
    public string rawChangelog = "";
    public string fullChangelogUrl = "";
}

public class ServiceInfo
{
    public string ip;
    public int port;
    public bool tcpServer;

    public ServiceInfo(string _ip, int _port, bool _tcpServer)
    {
        ip = _ip;
        port = _port;
        tcpServer = _tcpServer;
    }
}

public class LauncherTab
{
    public bool IsOpened;
    public FlowLayoutPanel panel;
    public virtual void Initialize() { }
    public virtual void OnTabOpen() { IsOpened = true; }
    public virtual void OnTabClose() { IsOpened = false; }
}