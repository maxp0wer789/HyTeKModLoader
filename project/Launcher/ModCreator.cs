﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static LauncherConfiguration;

namespace Launcher
{
    public partial class ModCreator : Form
    {
        Main defaultForm = null;
        public string devFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), FOLDERS_DEVENV);
        public string coremodPath;
        public string gamePath;

        public ModCreator(Main t)
        {
            Opacity = 0;
            FadeIn(this, 1).ContinueWith((v) => { });
            coremodPath = Path.Combine(Main.FOLDER_BINARIES, FILES_COREMOD);
            gamePath = Main.CurrentConfig.gamePath + "\\" + GAME_DATAFOLDERNAME;
            defaultForm = t;
            InitializeComponent();
            windowTitle.Text = GAME_NAME + " Mod C# Project Creator";
            info.Text = @"Welcome to the " + GAME_NAME + @" Mod Project Creator!

This tool will allow you to generate a development environment for your mod.

Mods are coded in the C#/CSharp language. If you want to learn more about how to make mods you can join our modding discord.

";
            projectpath.Text = devFolder;
            InitializeTheme();
        }

        public void InitializeTheme()
        {
#if GAME_IS_RAFT
            windowIcon.BackgroundImage = Properties.Resources.raft_rml_logo;
            pictureBox7.BackColor = THEME_TOPBAR_COLOR;
            windowIcon.BackColor = THEME_TOPBAR_COLOR;
            windowTitle.BackColor = THEME_TOPBAR_COLOR;
            closeBtn.BackColor = THEME_TOPBAR_COLOR;
            BackColor = THEME_BACKGROUND_COLOR;
            info.BackColor = THEME_BACKGROUND_COLOR;

            pictureBox1.BackColor = THEME_TOPBAR_COLOR;
            label1.BackColor = THEME_TOPBAR_COLOR;
            label3.BackColor = THEME_TOPBAR_COLOR;
            projectname.BackColor = THEME_BACKGROUND_COLOR;
            projectpath.BackColor = THEME_BACKGROUND_COLOR;
            label5.BackColor = THEME_TOPBAR_COLOR;
            label6.BackColor = THEME_TOPBAR_COLOR;
            dots.BackColor = THEME_BACKGROUND_COLOR;
            custompathbutton.BackColor = THEME_BACKGROUND_COLOR;
            custompathbutton.FlatAppearance.BorderColor = THEME_BACKGROUND_COLOR;
            custompathbutton.FlatAppearance.MouseOverBackColor = THEME_BACKGROUND_COLOR;
            custompathbutton.FlatAppearance.MouseDownBackColor = THEME_BACKGROUND_COLOR;
#endif
        }

        private async Task FadeIn(Form o, int interval = 80)
        {
            await Task.Delay(100);
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.09;
            }
            o.Opacity = 1;
        }

        private async void CloseForm(Form o, int interval = 80)
        {
            defaultForm.ReopenForm(1);
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.07;
            }
            o.Opacity = 0;
            this.Close();
        }

        bool mouseDown;
        int mouseX = 0, mouseY = 0;
        int mouseinX = 0, mouseinY = 0;

        private void DragMouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseinX = MousePosition.X - Bounds.X;
            mouseinY = MousePosition.Y - Bounds.Y;
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            CloseForm(this, 1);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(DISCORD_INVITEURL);
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            CreateProject();
        }

        public void CreateProject()
        {
            if (projectname.Text.Contains(" "))
            {
                MessageBox.Show("The project name title can't contain spaces!", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!IsFileNameCorrect(projectname.Text))
            {
                MessageBox.Show("The project name is invalid!", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrEmpty(projectname.Text))
            {
                MessageBox.Show("The project name can't contain blank characters.", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (projectname.Text.Length < 5 || projectname.Text.Length > 45)
            {
                MessageBox.Show("The project name should contain be between 5 and 45 characters.", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Directory.CreateDirectory(devFolder);
            string projectfolder = Path.Combine(devFolder, projectname.Text);
            if (Directory.Exists(projectfolder))
            {
                MessageBox.Show("A project with this name already exists!", "The name is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Directory.CreateDirectory(projectfolder);
            try
            {
                string managedDirectory = Path.Combine(Main.CurrentConfig.gamePath, GAME_DATAFOLDERNAME + "/Managed");
                if (Directory.Exists(managedDirectory))
                {
                    string dynamicReferences = "";

                    Directory.GetFiles(managedDirectory, "*.dll").ToList().ForEach((file) =>
                    {

                        dynamicReferences += "<Reference Include=\"" + Path.GetFileName(file) + "\">\n<HintPath>" + file + "</HintPath></Reference>";
                    });

                    Directory.GetFiles(Main.FOLDER_BINARIES, "*.dll").ToList().ForEach((file) =>
                    {

                        dynamicReferences += "<Reference Include=\"" + Path.GetFileName(file) + "\">\n<HintPath>" + file + "</HintPath></Reference>";
                    });

#if GAME_IS_RAFT
                    File.WriteAllText(Path.Combine(projectfolder, "build.bat"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles.build.bat")).Replace(".ghmod", ".rmod").Replace("GHML", "RML"));
#else
                    File.WriteAllText(Path.Combine(projectfolder, "build.bat"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles.build.bat")));
#endif
                    File.WriteAllText(Path.Combine(projectfolder, projectname.Text + ".sln"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.sln")).Replace("_MODNAME_", projectname.Text));

                    string secondfolder = Path.Combine(projectfolder, projectname.Text);
                    Directory.CreateDirectory(secondfolder);
                    File.WriteAllText(Path.Combine(secondfolder, projectname.Text + ".cs"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_._MODNAME_.cs")).Replace("_MODNAME_", projectname.Text));
                    File.WriteAllText(Path.Combine(secondfolder, projectname.Text + ".csproj"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_._MODNAME_.csproj")).Replace("_MODNAME_", projectname.Text).Replace("_DYNAMIC.REFERENCES_", dynamicReferences).Replace(".ghmod", ".rmod"));
                    string modinfo = Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.modinfo.json")).Replace("_MODNAME_", projectname.Text).Replace("_WEBSITENAME_", LauncherConfiguration.MODLOADER_SITE);
#if GAME_IS_RAFT
                    File.WriteAllText(Path.Combine(secondfolder, "modinfo.json"), modinfo.Replace("REQUIRED_BY_ALL_PLAYERS(RAFT_ONLY)", "\n  \"requiredByAllPlayers\": false,"));
#else
                    File.WriteAllText(Path.Combine(secondfolder, "modinfo.json"), modinfo.Replace("REQUIRED_BY_ALL_PLAYERS(RAFT_ONLY)", ""));
#endif
                    File.WriteAllBytes(Path.Combine(secondfolder, "banner.jpg"), Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.banner.jpg"));
                    File.WriteAllBytes(Path.Combine(secondfolder, "icon.png"), Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.icon.png"));
                }
                else
                {
                    MessageBox.Show("Your game directory is missing important files! Please verify your game files using steam and try again.", "Missing files!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            Process.Start(projectfolder);
            MessageBox.Show("Your mod project has been created in \n" + devFolder + "\nThe folder has been opened! Have a good time modding " + GAME_NAME + "!", "Your mod project has been successfully created!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            CloseForm(this, 1);
        }

        bool IsFileNameCorrect(string fileName)
        {
            return fileName.All(f => !Path.GetInvalidFileNameChars().Contains(f));
        }

        private void DragMouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - mouseinX;
                mouseY = MousePosition.Y - mouseinY;

                SetDesktopLocation(mouseX, mouseY);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dialog = new FolderSelectDialog
            {
                InitialDirectory = devFolder,
                Title = "Please select a folder to create the mod project inside"
            };
            if (dialog.Show(Handle))
            {
                string path = dialog.FileName;
                if (Directory.Exists(path))
                {
                    devFolder = path;
                    projectpath.Text = path;
                }
            }
        }
        private void dots_MouseClick(object sender, MouseEventArgs e)
        {
            button1_Click(null, null);
        }
    }
}
